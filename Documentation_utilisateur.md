# Documentation utilisateur

## 1. Prérequis

Pour pouvoir utiliser ce compilateur, il faut que Docker soit installé sur votre machine. Si vous ne l'avez pas, vous pouvez l'installer [ici](https://www.docker.com/get-started).

> Attention ce compilateur, ne génère du code assembleur que pour les processeurx x86, si votre procésseur n'est pas de ce type, une erreur aura lieu lors du passage du code assembleur à l'exécutable.

## 2. Compiler un programme C

A la racine du répertoire, à côté des dossiers `compiler` et `tests` vous pouvez trouver un script `compiler.sh` . Pour compiler votre programme C, il vous suffit d'exécutr cette commande :

```shell
./build.sh <programme.c> <executable>
```

`<programme.c>` correspond à votre programme à compiler et `<executable>` correspond au nom que votre exécutable une fois le prgramme compilé (vous pouvez le nommer comme bon vous semble).

## 3. Informations complémentaires

Ce compilateur du langage C ne peut compiler qu'un sous-ensemble de C, retrouvez ici la liste de ce que peut compiler `ifcc` :

* Gestion des types :  `int`
* Déclaration
  * Déclaration et de variables `int a;`
  * Déclarations multiples de variables `int a,b,c;`
  * Déclaration et définition de variables int dans tous le programme. `int a = 0;`
* Les affectations :
  * On peut affecter une expression à une left value value variable `a= 4+(5+a);`
  * Une affectation est une expression `a + (b=4) *5;`
  * affectation de constante négative
* Les fonctions:
  * appel de fonction
  * appel des procédures
  * définition de fonction
  * Un appel fonction est une expression `res = a + somme(4,3) *5;`
  * les fonctions peuvent accepter jusqu’à 6 paramètres
  * les paramètres sont des expressions `(4*5, somme(2,a))`
  * fonction récursive
  * `putchar()`
* Returnl
  * le return termine le bloc une fois rencontré
  * return d’une expression

* Structure de blocs {}
  * Imbrication de bloc {un bloc est une instruction}
  * porté de variable: accès à toute variables défini dans les blocs supérieurs 
* Expressions : 
  * opérateurs arithmétiques : *,+,-,=
  * opérateurs comparaison : <, > ,<= ,>= ,== ,!=
  * priorité d'opération () ←  * ←  +,-  ←   <, > ,<= ,>= ,== ,!= ←  =
* Structures de contrôles 
  * if, else,if else, while, for
  * Paramètres sont des expressions
  * If, for, while imbriqué
  * if, for, while dans le même bloc
* Gestion d’erreur : 
  * Double déclaration d’une variable
  * Variable utilisée mais non définie auparavant
  * (Warning) Variable déclarée mais non utilisée.