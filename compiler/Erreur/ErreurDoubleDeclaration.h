/*************************************************************************
                           ErreurDoubleDeclaration  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Interface de la classe <ErreurDoubleDeclaration> (fichier ErreurDoubleDeclaration.h) ----------------
#if ! defined ( ERREURDOUBLEDECLARATION_H )
#define ERREURDOUBLEDECLARATION_H

//--------------------------------------------------- Interfaces utilisées
#include "Erreur.h"
#include <iostream>
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <ErreurDoubleDeclaration>
//
//
//------------------------------------------------------------------------

class ErreurDoubleDeclaration : public Erreur
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques
    // type Méthode ( liste des paramètres );
    // Mode d'emploi :
    //
    // Contrat :
    //
    void afficherMessage(){std::cerr << "\033[1;31mDouble Déclaration pour la variable \033[0m'" << nom << "' : ";
    Erreur::afficherMessage();}
//-------------------------------------------- Constructeurs - destructeur
    ErreurDoubleDeclaration ( const ErreurDoubleDeclaration & unErreurDoubleDeclaration );
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //

    ErreurDoubleDeclaration (int l, int c, std::string nom) : Erreur(l, c, false), nom(nom){}
    // Mode d'emploi :
    //
    // Contrat :
    //

    ~ErreurDoubleDeclaration ( );
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE

protected:
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés
    std::string nom;
};

//-------------------------------- Autres définitions dépendantes de <ErreurDoubleDeclaration>

#endif // ERREURDOUBLEDECLARATION_H

