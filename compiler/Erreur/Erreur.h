/*************************************************************************
                           Erreur  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Interface de la classe <Erreur> (fichier Erreur.h) ----------------
#if ! defined ( ERREUR_H )
#define ERREUR_H

//--------------------------------------------------- Interfaces utilisées

//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <Erreur>
//
//
//------------------------------------------------------------------------

class Erreur
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques
    // type Méthode ( liste des paramètres );
    // Mode d'emploi :
    //
    // Contrat :
    //
	virtual void afficherMessage();
	//Afficher l'erreur appropriée 

    bool getEstUnWarning();

//-------------------------------------------- Constructeurs - destructeur
    Erreur ( const Erreur & unErreur );
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //

    Erreur (int l, int c, bool Warning);
    // Mode d'emploi :
    //
    // Contrat :
    //

    ~Erreur ( );
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE

protected:
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés
	int ligne;
	int colonne;
    bool estUnWarning;
};

//-------------------------------- Autres définitions dépendantes de <Erreur>

#endif // ERREUR_H

