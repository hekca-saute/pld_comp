/*************************************************************************
                           GestionnaireErreurs  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Interface de la classe <GestionnaireErreurs> (fichier GestionnaireErreurs.h) ----------------
#if ! defined ( GESTIONNAIREERREURS_H )
#define GESTIONNAIREERREURS_H

//--------------------------------------------------- Interfaces utilisées
#include <vector>
#include "Erreur.h"
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <GestionnaireErreurs>
//
//
//------------------------------------------------------------------------

class GestionnaireErreurs
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques
    // type Méthode ( liste des paramètres );
    // Mode d'emploi :
    //
    // Contrat :
    //
	void ajouterErreur(Erreur * e);

	void afficherErreurs();

    bool contientErreurFatale();

//-------------------------------------------- Constructeurs - destructeur
    GestionnaireErreurs ( const GestionnaireErreurs & unGestionnaireErreurs );
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //

    GestionnaireErreurs ( );
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~GestionnaireErreurs ( );
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE

protected:
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés
	vector<Erreur*> erreurs;
};

//-------------------------------- Autres définitions dépendantes de <GestionnaireErreurs>

#endif // GESTIONNAIREERREURS_H

