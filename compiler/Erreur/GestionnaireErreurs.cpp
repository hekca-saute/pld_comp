/*************************************************************************
                           GestionnaireErreurs  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation de la classe <GestionnaireErreurs> (fichier GestionnaireErreurs.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <iostream>
#include <vector>
using namespace std;

//------------------------------------------------------ Include personnel
#include "GestionnaireErreurs.h"
#include "Erreur.h"
//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC



//----------------------------------------------------- Méthodes publiques
// type GestionnaireErreurs::Méthode ( liste des paramètres )
// Algorithme :
//
//{
//} //----- Fin de Méthode

void GestionnaireErreurs::ajouterErreur(Erreur * e){
	erreurs.push_back(e);
}

void GestionnaireErreurs::afficherErreurs(){
	for(auto it:erreurs){
		it->afficherMessage();
	}
}

bool GestionnaireErreurs::contientErreurFatale(){
    for(auto it : erreurs){
        if(!it->getEstUnWarning()){
            return true;
        }
    }
    return false;
}

//-------------------------------------------- Constructeurs - destructeur
GestionnaireErreurs::GestionnaireErreurs ( const GestionnaireErreurs & unGestionnaireErreurs )
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au constructeur de copie de <GestionnaireErreurs>" << endl;
#endif
} //----- Fin de GestionnaireErreurs (constructeur de copie)


GestionnaireErreurs::GestionnaireErreurs ( )
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au constructeur de <GestionnaireErreurs>" << endl;
#endif
} //----- Fin de GestionnaireErreurs


GestionnaireErreurs::~GestionnaireErreurs ( )
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au destructeur de <GestionnaireErreurs>" << endl;
#endif
} //----- Fin de ~GestionnaireErreurs


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

