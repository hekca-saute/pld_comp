/*************************************************************************
                           ErreurVariableNonDeclaree  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Interface de la classe <ErreurVariableNonDeclaree> (fichier ErreurVariableNonDeclaree.h) ----------------
#if ! defined ( ERREURVARIABLENONDECLAREE_H )
#define ERREURVARIABLENONDECLAREE_H

//--------------------------------------------------- Interfaces utilisées
#include "Erreur.h"
#include <iostream>
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <ErreurVariableNonDeclaree>
//
//
//------------------------------------------------------------------------

class ErreurVariableNonDeclaree : public Erreur
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques
    // type Méthode ( liste des paramètres );
    // Mode d'emploi :
    //
    // Contrat :
    //
    void afficherMessage(){std::cerr << "\033[1;31mVariable\033[0m '"<< nom <<"' \033[1;31mnon déclarée\033[0m : ";
    Erreur::afficherMessage();}
//-------------------------------------------- Constructeurs - destructeur
    ErreurVariableNonDeclaree ( const ErreurVariableNonDeclaree & unErreurVariableNonDeclaree );
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //

    ErreurVariableNonDeclaree (int l, int c, std::string nom) : Erreur(l, c, false), nom(nom){}
    // Mode d'emploi :
    //
    // Contrat :
    //

    ~ErreurVariableNonDeclaree ( );
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE

protected:
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés
    std::string nom;
};

//-------------------------------- Autres définitions dépendantes de <ErreurVariableNonDeclaree>

#endif // ERREURVARIABLENONDECLAREE_H

