/*************************************************************************
                           ErreurVariableNonUtilisee  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Interface de la classe <ErreurVariableNonUtilisee> (fichier ErreurVariableNonUtilisee.h) ----------------
#if ! defined ( ERREURVARIABLENONUTILISEE_H )
#define ERREURVARIABLENONUTILISEE_H

//--------------------------------------------------- Interfaces utilisées
#include "Erreur.h"
#include <iostream>
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <ErreurVariableNonUtilisee>
//
//
//------------------------------------------------------------------------

class ErreurVariableNonUtilisee : public Erreur
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques
    // type Méthode ( liste des paramètres );
    // Mode d'emploi :
    //
    // Contrat :
    //
    void afficherMessage(){std::cerr << "\033[1;33m#Warning : variable \033[0m'" << nom << "' \033[1;33mnon utilisée \033[0m: ";
    Erreur::afficherMessage();}
//-------------------------------------------- Constructeurs - destructeur
    ErreurVariableNonUtilisee ( const ErreurVariableNonUtilisee & unErreurVariableNonUtilisee );
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //

    ErreurVariableNonUtilisee (int l, int c, std::string nom) : Erreur(l, c, true), nom(nom){}
    // Mode d'emploi :
    //
    // Contrat :
    //

    ~ErreurVariableNonUtilisee ( );
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE

protected:
//----------------------------------------------------- Méthodes protégées
    std::string nom;
//----------------------------------------------------- Attributs protégés

};

//-------------------------------- Autres définitions dépendantes de <ErreurVariableNonUtilisee>

#endif // ERREURVARIABLENONUTILISEE_H

