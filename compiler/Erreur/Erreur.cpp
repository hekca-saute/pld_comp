/*************************************************************************
                           Erreur  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation de la classe <Erreur> (fichier Erreur.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <iostream>
using namespace std;

//------------------------------------------------------ Include personnel
#include "Erreur.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

void Erreur::afficherMessage(){
	cout << "Erreur à la ligne : " <<ligne << ", colonne : " << colonne << endl;
}

bool Erreur::getEstUnWarning(){
    return estUnWarning;
}
//----------------------------------------------------- Méthodes publiques
// type Erreur::Méthode ( liste des paramètres )
// Algorithme :
//
//{
//} //----- Fin de Méthode

//-------------------------------------------- Constructeurs - destructeur
Erreur::Erreur ( const Erreur & unErreur )
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au constructeur de copie de <Erreur>" << endl;
#endif
} //----- Fin de Erreur (constructeur de copie)


Erreur::Erreur (int l, int c, bool Warning)
// Algorithme :
//
{
	ligne = l;
	colonne = c;
    estUnWarning = Warning;
#ifdef MAP
    cout << "Appel au constructeur de <Erreur>" << endl;
#endif
} //----- Fin de Erreur


Erreur::~Erreur ( )
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au destructeur de <Erreur>" << endl;
#endif
} //----- Fin de ~Erreur


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

