/*************************************************************************
                           Variable -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation de la classe <Variable> (fichier Variable.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <iostream>
using namespace std;

//------------------------------------------------------ Include personnel
#include "Variable.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques
// type Variable::Méthode ( liste des paramètres )
// Algorithme :
//
//{
//} //----- Fin de Méthode

void Variable::setAdresseMemoire(int mem)
// Algorithme :
//
{
    offset = mem;
} //----- Fin de Méthode

int Variable::getOffset ()
// Algorithme :
//
{
    //aEteUtilisee = true;
    return offset;
} //----- Fin de Méthode

string Variable::getType ()
// Algorithme :
//
{
    return type;
} //----- Fin de Méthode

bool Variable::getAEteUtilisee ()
// Algorithme :
//
{
    return aEteUtilisee;
} //----- Fin de Méthode

void Variable::utiliser ()
// Algorithme :
//
{
    aEteUtilisee = true;
}

int Variable::getLigne()
{
    return ligne;
}

int Variable::getColonne()
{
    return colonne;
}
//------------------------------------------------- Surcharge d'opérateurs
//Xxx & Variable::operator = ( const Xxx & unXxx )
// Algorithme :
//
//{
//} //----- Fin de operator =


//-------------------------------------------- Constructeurs - destructeur
//Variable::Variable ( const Xxx & unXxx )
// Algorithme :
//
//{
//#ifdef MAP
//    cout << "Appel au constructeur de copie de <Xxx>" << endl;
//#endif
//} //----- Fin de Xxx (constructeur de copie)
Variable::Variable ()
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au constructeur de <Variable>" << endl;
#endif
} //----- Fin de Xxx

Variable::Variable (string type, int l, int c)
// Algorithme :
//
{
    type = type;
    aEteUtilisee = false;
    ligne = l;
    colonne = c;
#ifdef MAP
    cout << "Appel au constructeur de <Variable>" << endl;
#endif
} //----- Fin de Xxx

Variable::Variable (string type)
// Algorithme :
//
{
    type = type;
    aEteUtilisee = false;
    ligne = 0;
    colonne = 0;
#ifdef MAP
    cout << "Appel au constructeur de <Variable>" << endl;
#endif
} //----- Fin de Xxx

Variable::~Variable ( )
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au destructeur de <Variable>" << endl;
#endif
} //----- Fin de ~Xxx


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

