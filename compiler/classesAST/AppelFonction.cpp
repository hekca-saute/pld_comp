/*************************************************************************
                           AppelFonction  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation de la classe <AppelFonction> (fichier AppelFonction.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <iostream>
using namespace std;

//------------------------------------------------------ Include personnel
#include "Variable.h"
#include "TableDesSymboles.h"
#include "Instruction.h"
#include "Expression.h"
#include "InstruRet.h"
#include "AppelFonction.h"


//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques
// type AppelFonction::Méthode ( liste des paramètres )
// Algorithme :
//
//{
//} //----- Fin de Méthode
string AppelFonction::creerAssembleurExpr (TableDesSymboles * variables)
// Algorithme :
//stocker les variable donné en parametre dans les registres puis dans la stack si plus de place
//call la fonction 
{
    //cout<<"creation assembleur d'appel fonction"<<endl;   //debug
    //creerAssembleur pour tout les affections de parametres
    std::vector<string> nomParams;
    for(std::vector<Expression*>::iterator it=parametres.begin(); it!=parametres.end(); ++it)
    {
        nomParams.push_back((*it)->creerAssembleurExpr(variables));
    }

    //Enregister les parametres dans les registre pour preparer l'appel de la fonction
    for(int i = 0; i<nomParams.size(); i++)
    {
        //std::cout<<nomParams[i]<<" nomParam et regParam  "<< regParam[i]<< endl;    //debug
        std::cout<<"movl	"<<variables->getOffset(nomParams[i])<<"(%rbp), %"<< regParam[i]<< "\n";
    }

    //Appel de la fonction et stockage de retour
    std::cout<<"callq	" << nomFonction << "\n";
    std::cout<<"movl	%eax, " <<variables->getOffset("_ret") <<"(%rbp)\n";
    return "_ret";


} //----- Fin de Méthode


void AppelFonction::creerAssembleur (TableDesSymboles * variables)
// Algorithme :
//
{
    this->creerAssembleurExpr(variables);
}//----- Fin de Méthode

TableDesSymboles * AppelFonction::parcourAST ( TableDesSymboles * variables )
{
    
    for(std::vector<Expression*>::iterator it=parametres.begin(); it!=parametres.end(); ++it)
    {
        (*it)->parcourAST(variables);
    }
    return variables;
}

//------------------------------------------------- Surcharge d'opérateurs
//AppelFonction & AppelFonction::operator = ( const AppelFonction & unAppelFonction )
// Algorithme :
//
//{
//} //----- Fin de operator =


//-------------------------------------------- Constructeurs - destructeur
//AppelFonction::AppelFonction ( const AppelFonction & unAppelFonction )
// Algorithme :
//
//{
//#ifdef MAP
//    cout << "Appel au constructeur de copie de <AppelFonction>" << endl;
//#endif
//} //----- Fin de AppelFonction (constructeur de copie)


AppelFonction::AppelFonction (string nomFonc, std::vector<Expression*> params)
// Algorithme :
//
{
    nomFonction = nomFonc;
    parametres = params;
    regParam = {"edi","esi","edx","ecx","r8d","r9d"};
#ifdef MAP
    cout << "Appel au constructeur de <AppelFonction>" << endl;
#endif
} //----- Fin de AppelFonction


AppelFonction::~AppelFonction ( )
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au destructeur de <AppelFonction>" << endl;
#endif
} //----- Fin de ~AppelFonction


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées


