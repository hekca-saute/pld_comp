/*************************************************************************
                             Instruction -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Interface de la classe <Instruction> (fichier Instruction.h) ----------------
#if ! defined ( INSTRUCTION_H )
#define INSTRUCTION_H

//--------------------------------------------------- Interfaces utilisées

//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types
//enum Type {INT,CHAR,VOID};
//------------------------------------------------------------------------
// Rôle de la classe <Instruction>
//
//
//------------------------------------------------------------------------

class Instruction 
{
//----------------------------------------------------------------- PUBLIC

public:
//variables Public
    static int nombreEtiquettes;
//----------------------------------------------------- Méthodes publiques
    // type Méthode ( liste des paramètres );
    // Mode d'emploi :
    //
    // Contrat :
    
    virtual TableDesSymboles * parcourAST(TableDesSymboles * tds){return tds;};
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual bool isReturn(){return false;};
    // Mode d'emploi :
    //
    // Contrat :
    //
    
    virtual void creerAssembleur (TableDesSymboles* variables){};
    // Mode d'emploi :
    //
    // Contrat :
    //

    int creerEtiquette();
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------- Surcharge d'opérateurs
    //Instruction & operator = ( const Instruction & unInstruction );
    // Mode d'emploi :
    //
    // Contrat :
    //


//-------------------------------------------- Constructeurs - destructeur
    //Instruction ( const Instruction & unInstruction );
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //

    Instruction ( );
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~Instruction ( );
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE

protected:
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés

};

//-------------------------------- Autres définitions dépendantes de <Instruction>

#endif // INSTRUCTION_H


