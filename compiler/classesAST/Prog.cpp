/*************************************************************************
                           Prog  -  description
                             -------------------
    début                : 22/03/2020
*************************************************************************/

//---------- Réalisation de la classe <Prog> (fichier Prog.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <iostream>
using namespace std;

//------------------------------------------------------ Include personnel
#include "Variable.h"
#include "TableDesSymboles.h"
#include "Instruction.h"
#include "Expression.h"
#include "ExpressionVar.h"
#include "Bloc.h"
#include "DefFonction.h"
#include "Prog.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques
// type Prog::Méthode ( liste des paramètres )
// Algorithme :
//
//{
//} //----- Fin de Méthode

void Prog::creerAssembleur(){

//std::vector<ifccParser::DefFonction *> fonctions = ctx->defFonction();
    for(list<DefFonction*>::iterator it=fonctions.begin(); it!=fonctions.end(); ++it)
    {
        (*it)->creerAssembleur();
    }
    
}
void Prog::parcourAST ()
// Algorithme :
//
{
   for(list<DefFonction*>::iterator it=fonctions.begin(); it!=fonctions.end(); ++it)
    {
        (*it)->parcourAST();
    } 
} //----- Fin de Méthode

void Prog::ajouterFonction (DefFonction * f){
    fonctions.push_back(f);
} //----- Fin de ajouterFonction

//-------------------------------------------- Constructeurs - destructeur
Prog::Prog ( )
// Algorithme :
//
{
    list<DefFonction> fonctions;
#ifdef MAP
    cout << "Appel au constructeur de <Prog>" << endl;
#endif
} //----- Fin de Prog


Prog::~Prog ( )
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au destructeur de <Prog>" << endl;
#endif
} //----- Fin de ~Prog


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

