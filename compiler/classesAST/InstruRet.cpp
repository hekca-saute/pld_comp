/*************************************************************************
                           InstruRet  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation de la classe <InstruRet> (fichier InstruRet.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <iostream>
using namespace std;

//------------------------------------------------------ Include personnel
#include "Variable.h"
#include "TableDesSymboles.h"
#include "Instruction.h"
#include "Expression.h"
#include "InstruRet.h"


//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques
// type InstruRet::Méthode ( liste des paramètres )
// Algorithme :
//
//{
//} //----- Fin de Méthode

void InstruRet::creerAssembleur (TableDesSymboles * variables)
// Algorithme :
//
{
    //cout<<"creation assembleur ret"<<endl;   //debug
    string varName = expr->creerAssembleurExpr(variables);
    std::cout<<"movl	"<<variables->getOffset(varName)<<"(%rbp), %eax\n";


} //----- Fin de Méthode

bool InstruRet::isReturn()
{
    return true;
}

TableDesSymboles * InstruRet::parcourAST (TableDesSymboles * tds)
// Algorithme :
//
{
    expr->parcourAST(tds);
    return tds;

} //----- Fin de Méthode

//------------------------------------------------- Surcharge d'opérateurs
//InstruRet & InstruRet::operator = ( const InstruRet & unInstruRet )
// Algorithme :
//
//{
//} //----- Fin de operator =


//-------------------------------------------- Constructeurs - destructeur
//InstruRet::InstruRet ( const InstruRet & unInstruRet )
// Algorithme :
//
//{
//#ifdef MAP
//    cout << "Appel au constructeur de copie de <InstrRet>" << endl;
//#endif
//} //----- Fin de InstruRet (constructeur de copie)


InstruRet::InstruRet (Expression * e )
// Algorithme :
//
{
expr = e;
#ifdef MAP
    cout << "Appel au constructeur de <InstruRet>" << endl;
#endif
} //----- Fin de InstruRet


InstruRet::~InstruRet ( )
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au destructeur de <InstruRet>" << endl;
#endif
} //----- Fin de ~InstruRet


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées


