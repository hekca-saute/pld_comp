/*************************************************************************
                           TableDesSymboles  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Interface de la classe <TableDesSymboles> (fichier TableDesSymboles.h) ----------------
#if ! defined ( TABLEDESSYMBOLES_H )
#define TABLEDESSYMBOLES_H

//--------------------------------------------------- Interfaces utilisées
#include <unordered_map>
#include <vector>
using namespace std;
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <TableDesSymboles>
// Contient les différentes expression valeurs ainsi que leurs adresses
//
//------------------------------------------------------------------------

class TableDesSymboles
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques
    // type Méthode ( liste des paramètres );
    // Mode d'emploi :
    //
    // Contrat :
    //

    bool ajouterVariable(std::string var, Variable varData);
        // Mode d'emploi : passer en paramètre la variable définie
        //
        // Contrat : Ajoute la variable à la table des symboles
        //
    
    int getOffset (std::string var);
    // Mode d'emploi :
    //
    // Contrat :
    //
    string getType (std::string var);
    // Mode d'emploi :
    //
    // Contrat :

    void afficherTable();
    // Méthode de débugage permettant d'afficher le contenu de la TDS.
    //
    // Contrat :
    //

    int getNombreConstFonction();
    // Mode d'emploi :
    //
    // Contrat :
    //

    void incrNombreConstFonction();
    // Mode d'emploi :
    //
    // Contrat :
    //

    int getNombreOperationFonction();
    // Mode d'emploi :
    //
    // Contrat :
    //

    void incrNombreOperationFonction();
    // Mode d'emploi :
    //
    // Contrat :
    //

    int getSpOffset();
    // Mode d'emploi :
    //
    // Contrat :
    //

    void setNombreVariableFonction(int nbrVarFct);
    // Mode d'emploi :
    //
    // Contrat :
    //

    void incrNombreVariableFonction();
    // Mode d'emploi :
    //
    // Contrat :
    //


    bool utiliserVariable(std::string nomVar);
    //Méthode permettant de définir une variable comme utilisée (gestion d'erreur)
    
    unordered_map<string,Variable> verifierUtilisation();
    //Méthode renvoyant l'ensemble des variables non utilisées.

    int getNombreVariableFonction();
    // Mode d'emploi :
    //
    // Contrat :
    //



//------------------------------------------------- Surcharge d'opérateurs
    //TableDesSymboles & operator = ( const TableDesSymboles & unTableDesSymboles );
    // Mode d'emploi :
    //
    // Contrat :
    //


//-------------------------------------------- Constructeurs - destructeur
    //TableDesSymboles ( const TableDesSymboles & unTableDesSymboles );
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //

    TableDesSymboles (TableDesSymboles * tableSup);
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~TableDesSymboles ( );
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE

protected:
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés
unordered_map<string,Variable> variables;
TableDesSymboles * tableSup;
int nombreConstFonction;
int nombreOperationFonction;
int nombreVariableFonction;
};

//-------------------------------- Autres définitions dépendantes de <TableDesSymboles>

#endif // TABLEDESSYMBOLES_H

