/*************************************************************************
                           Expression  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Interface de la classe <Expression> (fichier Expression.h) ----------------
#if ! defined ( EXPRESSION_H )
#define EXPRESSION_H

//--------------------------------------------------- Interfaces utilisées

//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <Expression>
//
//
//------------------------------------------------------------------------

class Expression : public Instruction
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques
    // type Méthode ( liste des paramètres );
    // Mode d'emploi :
    //
    // Contrat :
    //
    void creerAssembleur(TableDesSymboles * variables){}
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual string creerAssembleurExpr (TableDesSymboles * variables){return 0;}
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual int isConst (){return 0;}
    // Mode d'emploi :
    //
    // Contrat :
    //


    virtual TableDesSymboles * parcourAST(TableDesSymboles * tds){return tds;}
    // Mode d'emploi :
    //
    // Contrat :
    //


//------------------------------------------------- Surcharge d'opérateurs
    //Expression & operator = ( const Expression & unExpression );
    // Mode d'emploi :
    //
    // Contrat :
    //


//-------------------------------------------- Constructeurs - destructeur

    Expression ( );
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~Expression ( );
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE

protected:
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés

};

//-------------------------------- Autres définitions dépendantes de <Expression>

#endif // EXPRESSION_H

