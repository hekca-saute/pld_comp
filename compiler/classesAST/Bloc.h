/*************************************************************************
                             Bloc -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Interface de la classe <Bloc> (fichier Bloc.h) ----------------
#if ! defined ( BLOC_H )
#define BLOC_H


//--------------------------------------------------- Interfaces utilisées
#include <list>
#include <vector>
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <Bloc>
//
//
//------------------------------------------------------------------------


class Bloc : public Instruction 
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques
    // type Méthode ( liste des paramètres );
    // Mode d'emploi :
    //
    // Contrat :
    //

    void creerAssembleur (TableDesSymboles* variables);
    // Mode d'emploi :
    //
    // Contrat :
    //

    void ajouterInstruction (Instruction * instruction);
     // Mode d'emploi :
    //
    // Contrat :
    //

    TableDesSymboles * parcourAST(TableDesSymboles * tds);
    // Mode d'emploi :
    //
    // Contrat :
    //



//------------------------------------------------- Surcharge d'opérateurs
    //Bloc & operator = ( const Bloc & unBloc );
    // Mode d'emploi :
    //
    // Contrat :
    //


//-------------------------------------------- Constructeurs - destructeur
    //Bloc ( const Bloc & unBloc );
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //

    Bloc (TableDesSymboles * table);
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~Bloc ( );
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE

protected:
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés
list<Instruction*> instructions;
TableDesSymboles * variablesBloc;
};

//-------------------------------- Autres définitions dépendantes de <Bloc>

#endif // BLOC_H


