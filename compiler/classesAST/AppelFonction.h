/*************************************************************************
                             AppelFonction -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Interface de la classe <AppelFonction> (fichier AppelFonction.h) ----------------
#if ! defined ( APPELFONCTION_H )
#define APPELFONCTION_H


//--------------------------------------------------- Interfaces utilisées
#include <vector>
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <InstruRet>
//
//
//------------------------------------------------------------------------

class AppelFonction : public Expression
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques
    // type Méthode ( liste des paramètres );
    // Mode d'emploi :
    //
    // Contrat :
    //
    string creerAssembleurExpr (TableDesSymboles * variables);
    // Mode d'emploi :
    //
    // Contrat :
    //
    void creerAssembleur (TableDesSymboles * variables);
    // Mode d'emploi :
    //
    // Contrat :
    //

    
    TableDesSymboles * parcourAST(TableDesSymboles * tds);
    // Mode d'emploi :
    //
    // Contrat :
    //


//------------------------------------------------- Surcharge d'opérateurs
    //InstruRet & operator = ( const InstruRet & unInstruRet );
    // Mode d'emploi :
    //
    // Contrat :
    //


//-------------------------------------------- Constructeurs - destructeur
    //InstruRet ( const InstruRet & unInstruRet );
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //

    AppelFonction (string nomFonction, std::vector<Expression*> parametres);
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~AppelFonction ( );
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE

protected:
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés
std::string nomFonction;
std::vector<Expression*> parametres;
std::vector<std::string> regParam;
};

//-------------------------------- Autres définitions dépendantes de <AppelFonction>

#endif // APPELFONCTION_H


