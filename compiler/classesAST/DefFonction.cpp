/*************************************************************************
                           DefFonction  -  description
                             -------------------
    début                : 22/03/2020
*************************************************************************/

//---------- Réalisation de la classe <DefFonction> (fichier DefFonction.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <iostream>
using namespace std;

//------------------------------------------------------ Include personnel
#include "Variable.h"
#include "TableDesSymboles.h"
#include "Instruction.h"
#include "Expression.h"
#include "ExpressionVar.h"
#include "Bloc.h"
#include "DefFonction.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques
// type DefFonction::Méthode ( liste des paramètres )
// Algorithme :
//
//{
//} //----- Fin de Méthode


void DefFonction::parcourAST ()
// Algorithme :
//
{
    variablesFonction = bloc->parcourAST(nullptr);
    //variablesFonction->afficherTable();    //debug
    //cout<<"le nombre de variables fonction est = "<<to_string(variablesFonction->getNombreVariableFonction())<<endl;   //debug
    
} //----- Fin de Méthode

void DefFonction::ajouterInstruction (Instruction * instruction)
// Algorithme :
{
    bloc->ajouterInstruction(instruction);
} //----- Fin de Méthode

void DefFonction::creerAssembleur ()
// Algorithme :
{
    int SpOffset = variablesFonction->getSpOffset();
    std::cout<<
    ".globl	"<<nomFonction<<"\n"
    <<nomFonction<<": \n"
    "# prologue\n"
    "pushq %rbp          # save %rbp on the stack\n"
    "movq	%rsp, %rbp\n"
    "# body\n";
    //decalage du sp
    std::cout<<"subq	$"<<to_string(SpOffset)<<", %rsp\n";
    //recuperation des parametres
    int i=0;
    for(vector<pair<string,string>>::iterator it=paramFonction.begin(); it!=paramFonction.end(); ++it)
    {   
        //cout<<"recuepration d'une param de la fonction"<<endl;
        string nomParam = (*it).first;
        string type = (*it).second;
        std::cout<<"movl	%"<<regParam[i]<<", "<<variablesFonction->getOffset(nomParam)<<"(%rbp)\n";
        i++;
    } 
    //pour ajouter les variables globales, ajouter une TableDesSymboles a Prog a faire passer dans creerAssembleur(TdS)
    bloc->creerAssembleur(nullptr);

    std::cout<<
    "# epilogue\n"
    "addq	$"<< to_string(SpOffset) <<", %rsp\n"
    "popq %rbp            # restore %rbp from the stack\n"
    "ret                  # return to the caller (here the shell)\n\n\n";
    //variablesFonction->afficherTable();    //debug
} //----- Fin de Méthode

void DefFonction::setParamFonction (std::vector<pair<std::string, std::string>> paramFonc)
// Algorithme :
{
    paramFonction = paramFonc;
} //----- Fin de Méthode


//-------------------------------------------- Constructeurs - destructeur
DefFonction::DefFonction(string & nomFonc, string & t, Bloc * b)
// Algorithme :
//
{
    nomFonction = nomFonc;
    type = t;
    regParam = {"edi","esi","edx","ecx","r8d","r9d"};
    //Bloc *bloc = new Bloc;
    bloc = b;
    //list<Expression> paramFonction();

#ifdef MAP
    cout << "Appel au constructeur de <DefFonction>" << endl;
#endif
} //----- Fin de DefFonction


DefFonction::~DefFonction ( )
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au destructeur de <DefFonction>" << endl;
#endif
} //----- Fin de ~DefFonction


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

