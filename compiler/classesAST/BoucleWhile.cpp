/*************************************************************************
                           BoucleWhile  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation de la classe <BoucleWhile> (fichier BoucleWhile.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <iostream>
using namespace std;

//------------------------------------------------------ Include personnel
#include "Variable.h"
#include "TableDesSymboles.h"
#include "Instruction.h"
#include "Bloc.h"
#include "Expression.h"
#include "BoucleWhile.h"


//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques
// type BoucleWhile::Méthode ( liste des paramètres )
// Algorithme :
//
//{
//} //----- Fin de Méthode

void BoucleWhile::creerAssembleur (TableDesSymboles * variables)
// Algorithme :
//
{
    string prefix = ".JW_";
    cout<<prefix<<etiquetteDebut<<"_DEBUT:\n";
    string nomVar = condition->creerAssembleurExpr(variables);
    cout<<"cmpl $0, " <<variables->getOffset(nomVar)<<"(%rbp)\n";
    cout<<"je "<<prefix<<etiquetteFin<<"_FIN\n";
    bloc->creerAssembleur(variables);
    cout<<"jmp "<<prefix<<etiquetteDebut<<"_DEBUT\n";
    cout<<prefix<<etiquetteFin<<"_FIN:\n";

    
} //----- Fin de Méthode

TableDesSymboles * BoucleWhile::parcourAST (TableDesSymboles * tds)
// Algorithme :
//
{
    condition->parcourAST(tds);
    bloc->parcourAST(tds);
    return tds;
} //----- Fin de Méthode


//------------------------------------------------- Surcharge d'opérateurs
//BoucleWhile & BoucleWhile::operator = ( const BoucleWhile & unBoucleWhile )
// Algorithme :
//
//{
//} //----- Fin de operator =


//-------------------------------------------- Constructeurs - destructeur
//BoucleWhile::BoucleWhile ( const BoucleWhile & unBoucleWhile )
// Algorithme :
//
//{
//#ifdef MAP
//    cout << "Appel au constructeur de copie de <InstrRet>" << endl;
//#endif
//} //----- Fin de BoucleWhile (constructeur de copie)


BoucleWhile::BoucleWhile (Expression * expr, Bloc *b)
// Algorithme :
//
{
    condition= expr;
    bloc= b;
    etiquetteDebut =to_string(creerEtiquette());
    etiquetteFin = to_string(creerEtiquette());
#ifdef MAP
    cout << "Appel au constructeur de <BoucleWhile>" << endl;
#endif
} //----- Fin de BoucleWhile


BoucleWhile::~BoucleWhile ( )
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au destructeur de <BoucleWhile>" << endl;
#endif
} //----- Fin de ~BoucleWhile


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées


