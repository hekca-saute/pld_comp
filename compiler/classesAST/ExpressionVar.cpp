/*************************************************************************
                           ExpressionVar  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation de la classe <ExpressionVar> (fichier ExpressionVar.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <iostream>
using namespace std;

//------------------------------------------------------ Include personnel
#include "Variable.h"
#include "TableDesSymboles.h"
#include "Instruction.h"
#include "Expression.h"
#include "ExpressionVar.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques
// type ExpressionVar::Méthode ( liste des paramètres )
// Algorithme :
//
//{
//} //----- Fin de Méthode

string ExpressionVar::getNom()
{
    return nomVar;
}

string ExpressionVar::creerAssembleurExpr ( TableDesSymboles * variables )
{
    //cout<<"creation assembleur Var"<<endl;   //debug
    return nomVar;
}

TableDesSymboles * ExpressionVar::parcourAST ( TableDesSymboles * variables )
{
    return variables;
}


//------------------------------------------------- Surcharge d'opérateurs
//bool ExpressionVar::operator == ( const ExpressionVar & unExpressionVar )
//// Algorithme :
////
//{
//
//    return false;
//} //----- Fin de operator =


//-------------------------------------------- Constructeurs - destructeur
ExpressionVar::ExpressionVar(string nom)
// Algorithme :
//
{
    nomVar = nom;
#ifdef MAP
    cout << "Appel au constructeur de <ExpressionVar>" << endl;
#endif
} //----- Fin de ExpressionVar

ExpressionVar::ExpressionVar()
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au constructeur de <ExpressionVar>" << endl;
#endif
} //----- Fin de ExpressionVar


ExpressionVar::~ExpressionVar ( )
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au destructeur de <ExpressionVar>" << endl;
#endif
} //----- Fin de ~ExpressionVar


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

