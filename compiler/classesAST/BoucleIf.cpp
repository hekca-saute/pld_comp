/*************************************************************************
                           BoucleIf  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation de la classe <BoucleIf> (fichier BoucleIf.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <iostream>
using namespace std;

//------------------------------------------------------ Include personnel
#include "Variable.h"
#include "TableDesSymboles.h"
#include "Instruction.h"
#include "Bloc.h"
#include "Expression.h"
#include "BoucleIf.h"


//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques
// type BoucleIf::Méthode ( liste des paramètres )
// Algorithme :
//
//{
//} //----- Fin de Méthode

void BoucleIf::creerAssembleur (TableDesSymboles * variables)
// Algorithme :
//
{
    //std::cout<<"we are in creerAssembleur de BoucleIf, list blocs = "<<blocs.size()<<std::endl;  //debug
    string prefix = ".JIF_";
    for(list<pair<Expression*,Bloc*>>::iterator it = blocs.begin(); it!=blocs.end(); ++it)
    {  
        string etiquetteFinBloc = to_string(creerEtiquette());
        string nomVar = it->first->creerAssembleurExpr(variables);
        cout<<"cmpl $0, " <<variables->getOffset(nomVar)<<"(%rbp)\n";
        cout<<"je "<<prefix<<etiquetteFinBloc<<"\n";
        it->second->creerAssembleur(variables);
        cout<<"jmp "<<prefix<<etiquetteFin<<"_FIN\n";
        cout<<prefix<<etiquetteFinBloc<<":\n";

    }
    cout<<prefix<<etiquetteFin<<"_FIN:\n";
    
} //----- Fin de Méthode

TableDesSymboles * BoucleIf::parcourAST (TableDesSymboles * variables)
// Algorithme :
//
{
    for(list<pair<Expression*,Bloc*>>::iterator it = blocs.begin(); it!=blocs.end(); ++it)
    {
        TableDesSymboles *  tableActuel =  it->first->parcourAST(variables);
        TableDesSymboles *  tableDuNouveauBloc = it->second->parcourAST(variables);
    }
    return variables;
} //----- Fin de Méthode


//------------------------------------------------- Surcharge d'opérateurs
//BoucleIf & BoucleIf::operator = ( const BoucleIf & unBoucleIf )
// Algorithme :
//
//{
//} //----- Fin de operator =


//-------------------------------------------- Constructeurs - destructeur
//BoucleIf::BoucleIf ( const BoucleIf & unBoucleIf )
// Algorithme :
//
//{
//#ifdef MAP
//    cout << "Appel au constructeur de copie de <InstrRet>" << endl;
//#endif
//} //----- Fin de BoucleIf (constructeur de copie)


BoucleIf::BoucleIf (list<pair<Expression*,Bloc*>> b)
// Algorithme :
//
{
    blocs = b;
    etiquetteFin = to_string(creerEtiquette());
#ifdef MAP
    cout << "Appel au constructeur de <BoucleIf>" << endl;
#endif
} //----- Fin de BoucleIf


BoucleIf::~BoucleIf ( )
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au destructeur de <BoucleIf>" << endl;
#endif
} //----- Fin de ~BoucleIf


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées


