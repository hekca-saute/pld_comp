/*************************************************************************
                           DefFonction  -  description
                             -------------------
    début                : 22/03/2020
*************************************************************************/

//---------- Interface de la classe <DefFonction> (fichier DefFonction.h) ----------------
#if ! defined ( DEFFONCTION_H )
#define DEFFONCTION_H

//--------------------------------------------------- Interfaces utilisées

//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <DefFonction>
//
//
//------------------------------------------------------------------------

class DefFonction
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques
    // type Méthode ( liste des paramètres );
    // Mode d'emploi :
    //
    // Contrat :
    //

    void parcourAST();
    // Mode d'emploi :
    //
    // Contrat :
    //

    void creerAssembleur ();
    // Mode d'emploi :
    //
    // Contrat :
    //
    void ajouterInstruction ( Instruction * intruction);
    // Mode d'emploi :
    // Contrat :
    //
    
    void setParamFonction (std::vector<pair<std::string, std::string>> paramFonc);
    // Mode d'emploi :
    //
    // Contrat :
    //

    void setVariablesFonction (TableDesSymboles * variableFonction);
    // Mode d'emploi :
    //
    // Contrat :
    //


//------------------------------------------------------------------ Constructeur Destructeur

    DefFonction(std::string & id, std::string & type, Bloc * bloc);
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~DefFonction ( );
    // Mode d'emploi :
    //
    // Contrat :
    //


//------------------------------------------------------------------ PRIVE

protected:
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés
    std::string nomFonction;
    std::string type;
    Bloc *bloc;
    //pair<nomvar,type>
    std::vector<pair<std::string, std::string>> paramFonction;
    std::vector<std::string> regParam;
    TableDesSymboles * variablesFonction;
};

//-------------------------------- Autres définitions dépendantes de <DefFonction>

#endif // DEFFONCTION_H

