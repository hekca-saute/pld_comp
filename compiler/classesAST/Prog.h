/*************************************************************************
                           Prog  -  description
                             -------------------
    début                : 22/03/2020
*************************************************************************/

//---------- Interface de la classe <Prog> (fichier Prog.h) ----------------
#if ! defined ( PROG_H )
#define PROG_H

//--------------------------------------------------- Interfaces utilisées

//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <Prog>
//
//
//------------------------------------------------------------------------

class Prog
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques
    // type Méthode ( liste des paramètres );
    // Mode d'emploi :
    //
    // Contrat :
    //
    void ajouterFonction(DefFonction * f);
    // Mode d'emploi :
    //
    // Contrat :
    //

    void creerAssembleur();
    // Mode d'emploi :
    //
    // Contrat :
    //
    void parcourAST();
    // Mode d'emploi :
    //
    // Contrat :
    //

    void verifierUtilisationVariables();

//-------------------------------------------- Constructeurs - destructeur
    Prog ( );
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~Prog ( );
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE

protected:
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés
   std::list<DefFonction *> fonctions;
};

//-------------------------------- Autres définitions dépendantes de <Xxx>

#endif // PROG_H

