/*************************************************************************
                           Expression  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation de la classe <Expression> (fichier Expression.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <iostream>
using namespace std;

//------------------------------------------------------ Include personnel
#include "Variable.h"
#include "TableDesSymboles.h"
#include "Instruction.h"
#include "Expression.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques
// type Expression::Méthode ( liste des paramètres )
// Algorithme :
//
//{
//} //----- Fin de Méthode


//------------------------------------------------- Surcharge d'opérateurs
//Expression & Expression::operator = ( const Expression & unExpression )
// Algorithme :
//
//{
//} //----- Fin de operator =


//-------------------------------------------- Constructeurs - destructeur
Expression::Expression ( )
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au constructeur de <Expression>" << endl;
#endif
} //----- Fin de Expression


Expression::~Expression ( )
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au destructeur de <Expression>" << endl;
#endif
} //----- Fin de ~Expression


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

