/*************************************************************************
                           TableDesSymboles  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation de la classe <TableDesSymboles> (fichier TableDesSymboles.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;
//------------------------------------------------------ Include personnel
#include "Variable.h"
#include "TableDesSymboles.h"
//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques
// type TableDesSymboles::Méthode ( liste des paramètres )
// Algorithme :
//
//{
//} //----- Fin de Méthode

int TableDesSymboles::getOffset (string var)
// Algorithme :
//
{
    auto it = variables.find(var);
    if(it != variables.end())
    {
        Variable data = it->second;
        return data.getOffset();
    }else if(tableSup != nullptr)
    {
        return tableSup->getOffset(var); 
    }else
    {
        return -1; //gestion D'erreur
    }
} //----- Fin de Méthode

string TableDesSymboles::getType (string var)
// Algorithme :
//
{
    Variable data = variables[var];
    return data.getType();
} //----- Fin de Méthode

void TableDesSymboles::afficherTable()
// Algorithme :
//
{
    for(auto element : variables)
    {
       cout << element.first << " " << element.second.getType() << element.second.getOffset() <<"Valeur bool : " << element.second.getAEteUtilisee() <<  "\n";
    }
} //----- Fin de afficherTable


void TableDesSymboles::incrNombreConstFonction()
{
    if (tableSup !=nullptr)
    {
        tableSup->incrNombreConstFonction();
    }else
    {
        nombreConstFonction++ ;
    }
}

int TableDesSymboles::getNombreConstFonction()
{
    if (tableSup !=nullptr)
    {
        return tableSup->getNombreConstFonction();
    }else
    {
        return nombreConstFonction;
    }
}


void TableDesSymboles::incrNombreOperationFonction()
{
    if (tableSup !=nullptr)
    {
        tableSup->incrNombreOperationFonction();
    }else
    {
        nombreOperationFonction++ ;
    }
}

int TableDesSymboles::getNombreOperationFonction()
{
    if (tableSup !=nullptr)
    {
        return tableSup->getNombreOperationFonction();
    }else
    {
        return nombreOperationFonction;
    }
}
int TableDesSymboles::getSpOffset()
{
    //cout<<variables.size()<<"    "<< nombreConst<<"    "<< nombreOperation;    //debug
    int nombreVarTotal = 0;
    nombreVarTotal = getNombreVariableFonction() + getNombreConstFonction() + getNombreOperationFonction();
    int offset;
    if ((nombreVarTotal*4)%16==0)
    {
        offset = nombreVarTotal*4;
    }else
    {
        offset = 16*(((nombreVarTotal)*4)/16+1);
    }
    return offset;
}

bool TableDesSymboles::ajouterVariable(string var, Variable varData)
// Algorithme :
//
{
    bool added = true;
    auto it = variables.find(var);
    if(it != variables.end())
    {
        added = false;
    }
    else
    {
        int nombreVarTot = getNombreVariableFonction();
        varData.setAdresseMemoire(-4 -4*nombreVarTot);
        variables[var]= varData;
        incrNombreVariableFonction();
    }
    
    return added;
}

unordered_map<string,Variable> TableDesSymboles::verifierUtilisation(){
    unordered_map<string,Variable> variablesRetour;

    for(auto it:variables)
    {
        if(!it.second.getAEteUtilisee())
        {
            variablesRetour[it.first] = it.second;
        }
    }
    return variablesRetour;
}

bool TableDesSymboles::utiliserVariable(string nomVar){
    auto it = variables.find(nomVar);
    if(it != variables.end())
    {
        variables[nomVar].utiliser();
        return true;
    }else if(tableSup!=nullptr)
    {
        return tableSup->utiliserVariable(nomVar);
    }
    return false;
}
int TableDesSymboles::getNombreVariableFonction()
{
    if (tableSup !=nullptr)
    {
        return tableSup->getNombreVariableFonction();
    }else
    {
        return nombreVariableFonction;
    }
}

void TableDesSymboles::incrNombreVariableFonction()
{
    if (tableSup !=nullptr)
    {
        tableSup->incrNombreVariableFonction();
    }else
    {
        nombreVariableFonction++;
    }
}

void TableDesSymboles::setNombreVariableFonction(int nbrVarTot)
{
    nombreVariableFonction = nbrVarTot;
}

//------------------------------------------------- Surcharge d'opérateurs
//TableDesSymboles & TableDesSymboles::operator = ( const TableDesSymboles & unTableDesSymboles )
// Algorithme :
//
//{
//} //----- Fin de operator =


//-------------------------------------------- Constructeurs - destructeur
//TableDesSymboles::TableDesSymboles ( const TableDesSymboles & unTableDesSymboles )
// Algorithme :
//
//{
//#ifdef MAP
//    cout << "Appel au constructeur de copie de <TableDesSymboles>" << endl;
//#endif
//} //----- Fin de TableDesSymboles (constructeur de copie)


TableDesSymboles::TableDesSymboles (TableDesSymboles * t)
// Algorithme :
//
{
    tableSup = t;
    nombreVariableFonction = 0;
    nombreConstFonction = 0;
    nombreOperationFonction =0;
#ifdef MAP
    cout << "Appel au constructeur de <TableDesSymboles>" << endl;
#endif
} //----- Fin de TableDesSymboles


TableDesSymboles::~TableDesSymboles ( )
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au destructeur de <TableDesSymboles>" << endl;
#endif
} //----- Fin de ~TableDesSymboles



//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

