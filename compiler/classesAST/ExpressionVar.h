/*************************************************************************
                           ExpressionVar  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Interface de la classe <ExpressionVar> (fichier ExpressionVar.h) ----------------
#if ! defined ( EXPRESSIONVAR_H )
#define EXPRESSIONVAR_H

//--------------------------------------------------- Interfaces utilisées

//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <ExpressionVar>
//
//
//------------------------------------------------------------------------

class ExpressionVar : public Expression
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques
    // type Méthode ( liste des paramètres );
    // Mode d'emploi :
    //
    // Contrat :
    //
    std::string getNom();

    string creerAssembleurExpr ( TableDesSymboles * variables );
        // Mode d'emploi :
        //
        // Contrat :
        //
    TableDesSymboles * parcourAST(TableDesSymboles * tds);
    // Mode d'emploi :
    //
    // Contrat :
    //

////------------------------------------------------- Surcharge d'opérateurs
//    bool operator == ( const ExpressionVar & unExpressionVar );
//    // Mode d'emploi :
//    //
//    // Contrat :
//    //


//-------------------------------------------- Constructeurs - destructeur

    ExpressionVar(std::string nom);
    ExpressionVar();

    virtual ~ExpressionVar ( );
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE

protected:
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés
std::string nomVar;
};

//-------------------------------- Autres définitions dépendantes de <ExpressionVar>

#endif // EXPRESSIONVAR_H

