/*************************************************************************
                             InstruRet -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Interface de la classe <InstruRet> (fichier InstruRet.h) ----------------
#if ! defined ( INSTRURET_H )
#define INSTRURET_H


//--------------------------------------------------- Interfaces utilisées

//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <InstruRet>
//
//
//------------------------------------------------------------------------

class InstruRet : public Instruction 
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques
    // type Méthode ( liste des paramètres );
    // Mode d'emploi :
    //
    // Contrat :
    //
    void creerAssembleur (TableDesSymboles * variables);
    // Mode d'emploi :
    //
    // Contrat :
    //
    
     TableDesSymboles * parcourAST(TableDesSymboles * tds);
    // Mode d'emploi :
    //
    // Contrat :
    //

    bool isReturn();
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------- Surcharge d'opérateurs
    //InstruRet & operator = ( const InstruRet & unInstruRet );
    // Mode d'emploi :
    //
    // Contrat :
    //


//-------------------------------------------- Constructeurs - destructeur
    //InstruRet ( const InstruRet & unInstruRet );
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //

    InstruRet (Expression * expr);
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~InstruRet ( );
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE

protected:
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés
Expression * expr;
};

//-------------------------------- Autres définitions dépendantes de <InstruRet>

#endif // INSTRURET_H


