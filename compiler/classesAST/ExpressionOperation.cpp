/*************************************************************************
                           ExpressionOperation  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation de la classe <ExpressionOperation> (fichier ExpressionOperation.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <iostream>
using namespace std;

//------------------------------------------------------ Include personnel
#include "Variable.h"
#include "TableDesSymboles.h"
#include "Instruction.h"
#include "Expression.h"
#include "ExpressionVar.h"
#include "ExpressionOperation.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques
// type ExpressionOperation::Méthode ( liste des paramètres )
// Algorithme :
//
//{
//} //----- Fin de Méthode

void ExpressionOperation::creerAssembleur (TableDesSymboles * variables)
// Algorithme :
// 
{
    creerAssembleurExpr(variables);  
} //----- Fin de Méthode

string ExpressionOperation::creerAssembleurExpr ( TableDesSymboles * variables )
{   
    //cout<<"debut de creation d'assembleurExpr de Operation"<<endl;   //debug
    string var1 = expression1->creerAssembleurExpr(variables);
    //cout<<"apres la recuperation de la premiere variable"<<endl;   //debug
    string var2 = expression2->creerAssembleurExpr(variables);
    int offset1 = variables->getOffset(var1);
    int offset2 = variables->getOffset(var2);
    if(symboleOperation=="=")
    {
        //cout<<"creation assembleur d'affectation"<<endl;   //debug
        cout<<"movl	"<<offset2<<"(%rbp), %eax\n";
        cout<<"movl    %eax, "<<offset1<<"(%rbp)\n";
        return var1;
    }
    
    cout<<"movl	"<<offset1<<"(%rbp), %edx\n";
    cout<<"movl	"<<offset2<<"(%rbp), %eax\n";

    if(symboleOperation=="+")
    {
       //cout<<"creation assembleur plus"<<endl;   //debug
        cout<<"addl	%edx, %eax\n";
    } else if(symboleOperation=="-")
    {
        //cout<<"creation assembleur moins"<<endl;   //debug
        cout<<"subl	%eax, %edx\n";
    } else if(symboleOperation=="*")
    {
       // cout<<"creation assembleur mult"<<endl;   //debug
        cout<<"imul %edx, %eax\n";
    } else if(symboleOperation=="/")
    {
        //cout<<"creation assembleur div"<<endl;   //debug
        cout<<"ctld\n";
        cout<< "idivl %edx\n";
    } else if(symboleOperation==">" || symboleOperation=="<" || symboleOperation==">=" || symboleOperation=="<=" || symboleOperation=="==" || symboleOperation=="!=")
    {
        cout<<"cmpl    %edx, %eax\n";
        //Attention, ici il faut inverser les flags pour les <, <=, >, >=. Donc pour < (less) on utilise le flag ge (greater or equal) car a less b envoie 0 si a<b
        //Par contre, pour == et !=, c'est respecté car == renvoie 1 si égal et 0 si diff, et l'inverse pour !=
        if(symboleOperation==">")
        {
            cout<<"setl    %al\n";
        } else if(symboleOperation==">=")
        {
            cout<<"setle    %al\n";
        } else if(symboleOperation=="<")
        {
            cout<<"setg    %al\n";
        } else if(symboleOperation=="<=")
        {
            cout<<"setge    %al\n";
        } else if(symboleOperation=="==")
        {
            cout<<"sete    %al\n";
        } else if(symboleOperation=="!=")
        {
            cout<<"setne    %al\n";
        }
        cout<<"movzbl  %al, %eax\n";
    }

    Variable nouvelleVar = Variable("int");
    string nomNouvelleVar = to_string(variables->getNombreVariableFonction()) + "inter";
    variables->ajouterVariable(nomNouvelleVar,nouvelleVar);
    int newOffset = variables->getOffset(nomNouvelleVar);
    string registreRetour = "eax";
    if(symboleOperation=="-"){
        registreRetour = "edx";
    }

    cout<<"movl	%"<<registreRetour<<", "<<newOffset<<"(%rbp)\n";

    return nomNouvelleVar;
}

TableDesSymboles * ExpressionOperation::parcourAST ( TableDesSymboles * variables )
{
    expression1->parcourAST(variables);
    expression2->parcourAST(variables);
     if(symboleOperation=="+")
    {
        variables->incrNombreOperationFonction();
    } else if(symboleOperation=="-")
    {
        variables->incrNombreOperationFonction();
    } else if(symboleOperation=="*")
    {
        variables->incrNombreOperationFonction();
    } else if(symboleOperation=="/")
    {
        variables->incrNombreOperationFonction();
    }else if (symboleOperation==">" || symboleOperation=="<" || symboleOperation==">=" || symboleOperation=="<=" || symboleOperation=="==" || symboleOperation=="!=")
    {
        variables->incrNombreOperationFonction();
    }
    return variables;
}


//------------------------------------------------- Surcharge d'opérateurs
//ExpressionOperation & ExpressionOperation::operator = ( const ExpressionOperation & unExpressionOperation )
// Algorithme :
//
//{
//} //----- Fin de operator =


//-------------------------------------------- Constructeurs - destructeur
ExpressionOperation::ExpressionOperation(string symb, Expression * expr1, Expression * expr2)
// Algorithme :
//
{
    expression1 = expr1;
    expression2 = expr2;
    symboleOperation = symb;
#ifdef MAP
    cout << "Appel au constructeur de <ExpressionOperation>" << endl;
#endif
} //----- Fin de ExpressionOperation



ExpressionOperation::~ExpressionOperation ( )
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au destructeur de <ExpressionOperation>" << endl;
#endif
} //----- Fin de ~ExpressionOperation


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

