/*************************************************************************
                             BoucleFor -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Interface de la classe <BoucleFor> (fichier BoucleFor.h) ----------------
#if ! defined ( BOUCLEFOR_H )
#define BOUCLEFOR_H


//--------------------------------------------------- Interfaces utilisées

//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <BoucleFor>
//
//
//------------------------------------------------------------------------

class BoucleFor : public Instruction 
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques
    // type Méthode ( liste des paramètres );
    // Mode d'emploi :
    //
    // Contrat :
    //
    void creerAssembleur (TableDesSymboles * variables);
    // Mode d'emploi :
    //
    // Contrat :
    //
    
     TableDesSymboles * parcourAST(TableDesSymboles * tds);
    // Mode d'emploi :
    //
    // Contrat :
    //


//------------------------------------------------- Surcharge d'opérateurs
    //BoucleFor & operator = ( const BoucleFor & unBoucleFor );
    // Mode d'emploi :
    //
    // Contrat :
    //


//-------------------------------------------- Constructeurs - destructeur
    //BoucleFor ( const BoucleFor & unBoucleFor );
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //
    
    BoucleFor (Expression * expr1,Expression * condition, Expression * incrementation, Bloc *bloc);
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~BoucleFor ( );
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE

protected:
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés

Expression * init;
Expression * condition;
Expression * incrementation;
Bloc *bloc;
string etiquetteDebut;
string etiquetteFin;
};

//-------------------------------- Autres définitions dépendantes de <BoucleFor>

#endif // BOUCLEFOR_H


