/*************************************************************************
                             BoucleWhile -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Interface de la classe <BoucleWhile> (fichier BoucleWhile.h) ----------------
#if ! defined ( BOUCLEWHILE_H )
#define BOUCLEWHILE_H


//--------------------------------------------------- Interfaces utilisées

//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <BoucleWhile>
//
//
//------------------------------------------------------------------------

class BoucleWhile : public Instruction 
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques
    // type Méthode ( liste des paramètres );
    // Mode d'emploi :
    //
    // Contrat :
    //
    void creerAssembleur (TableDesSymboles * variables);
    // Mode d'emploi :
    //
    // Contrat :
    //
    
     TableDesSymboles * parcourAST(TableDesSymboles * tds);
    // Mode d'emploi :
    //
    // Contrat :
    //


//------------------------------------------------- Surcharge d'opérateurs
    //BoucleWhile & operator = ( const BoucleWhile & unBoucleWhile );
    // Mode d'emploi :
    //
    // Contrat :
    //


//-------------------------------------------- Constructeurs - destructeur
    //BoucleWhile ( const BoucleWhile & unBoucleWhile );
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //
    
    BoucleWhile (Expression * expr, Bloc *bloc);
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~BoucleWhile ( );
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE

protected:
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés

Expression * condition;
Bloc * bloc;
string etiquetteFin;
string etiquetteDebut;
};

//-------------------------------- Autres définitions dépendantes de <BoucleWhile>

#endif // BOUCLEWHILE_H


