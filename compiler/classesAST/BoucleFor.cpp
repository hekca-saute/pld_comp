/*************************************************************************
                           BoucleFor  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation de la classe <BoucleFor> (fichier BoucleFor.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <iostream>
using namespace std;

//------------------------------------------------------ Include personnel
#include "Variable.h"
#include "TableDesSymboles.h"
#include "Instruction.h"
#include "Bloc.h"
#include "Expression.h"
#include "BoucleFor.h"


//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques
// type BoucleFor::Méthode ( liste des paramètres )
// Algorithme :
//
//{
//} //----- Fin de Méthode

void BoucleFor::creerAssembleur (TableDesSymboles * variables)
// Algorithme :
//
{
    string prefix = ".JF_";
    string nomVarInit = init->creerAssembleurExpr(variables);
    cout<<prefix<<etiquetteDebut<<"_DEBUT:\n";
    string nomVarCondition = condition->creerAssembleurExpr(variables);
    cout<<"cmpl $0, " <<variables->getOffset(nomVarCondition)<<"(%rbp)\n";
    cout<<"je "<<prefix<<etiquetteFin<<"_FIN\n";
    bloc->creerAssembleur(variables);
    string nomVarIncrementation = incrementation->creerAssembleurExpr(variables);
    cout<<"jmp "<<prefix<<etiquetteDebut<<"_DEBUT\n";
    cout<<prefix<<etiquetteFin<<"_FIN:\n";
} //----- Fin de Méthode

TableDesSymboles * BoucleFor::parcourAST (TableDesSymboles * tds)
// Algorithme :
//
{
    init->parcourAST(tds);
    condition->parcourAST(tds);
    incrementation->parcourAST(tds);
    bloc->parcourAST(tds);
    return tds;
} //----- Fin de Méthode


//------------------------------------------------- Surcharge d'opérateurs
//BoucleFor & BoucleFor::operator = ( const BoucleFor & unBoucleFor )
// Algorithme :
//
//{
//} //----- Fin de operator =


//-------------------------------------------- Constructeurs - destructeur
//BoucleFor::BoucleFor ( const BoucleFor & unBoucleFor )
// Algorithme :
//
//{
//#ifdef MAP
//    cout << "Appel au constructeur de copie de <InstrRet>" << endl;
//#endif
//} //----- Fin de BoucleFor (constructeur de copie)


BoucleFor::BoucleFor (Expression * e1,Expression * e2, Expression * e3, Bloc *b)
// Algorithme :
//
{
    init= e1;
    condition = e2;
    incrementation = e3;
    bloc= b;
    etiquetteDebut =  to_string(creerEtiquette());
    etiquetteFin =  to_string(creerEtiquette());
#ifdef MAP
    cout << "Appel au constructeur de <BoucleFor>" << endl;
#endif
} //----- Fin de BoucleFor


BoucleFor::~BoucleFor ( )
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au destructeur de <BoucleFor>" << endl;
#endif
} //----- Fin de ~BoucleFor


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées


