/*************************************************************************
                           ExpressionOperation  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Interface de la classe <ExpressionOperation> (fichier ExpressionOperation.h) ----------------
#if ! defined ( EXPRESSIONOPERATION_H )
#define EXPRESSIONOPERATION_H

//--------------------------------------------------- Interfaces utilisées

//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <ExpressionOperation>
//
//
//------------------------------------------------------------------------

class ExpressionOperation : public Expression
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques

    // type Méthode ( liste des paramètres );
    // Mode d'emploi :
    //
    // Contrat :
    //

    string creerAssembleurExpr ( TableDesSymboles * variables );
    // Mode d'emploi :
    //
    // Contrat :
    //
    void creerAssembleur(TableDesSymboles * variables);
    // Mode d'emploi :
    //
    // Contrat :
    //

    TableDesSymboles * parcourAST(TableDesSymboles * tds);
    // Mode d'emploi :
    //
    // Contrat :
    //
    

//------------------------------------------------- Surcharge d'opérateurs
    ExpressionOperation & operator = ( const ExpressionOperation & unExpressionOperation );
    // Mode d'emploi :
    //
    // Contrat :
    //


//-------------------------------------------- Constructeurs - destructeur
    ExpressionOperation(std::string symboleOperation, Expression * expression1, Expression * expression2);

    virtual ~ExpressionOperation ( );
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE

protected:
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés
std::string symboleOperation;
Expression * expression1;
Expression * expression2;
};

//-------------------------------- Autres définitions dépendantes de <ExpressionOperation>

#endif // EXPRESSIONOPERATION_H

