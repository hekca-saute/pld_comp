/*************************************************************************
                           Bloc  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation de la classe <Bloc> (fichier Bloc.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <iostream>
#include <list>
#include <vector>
using namespace std;
//------------------------------------------------------ Include personnel
#include "Variable.h"
#include "TableDesSymboles.h"
#include "Instruction.h"
#include "Expression.h"
#include "ExpressionVar.h"
#include "Bloc.h"
//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques
// type Bloc::Méthode ( liste des paramètres )
// Algorithme :
//
//{
//} //----- Fin de Méthode

TableDesSymboles * Bloc::parcourAST (TableDesSymboles * tds)
// Algorithme :
//
{
    for(list<Instruction*>::iterator it=instructions.begin(); it!=instructions.end(); ++it)
    {
        TableDesSymboles * t = (*it)->parcourAST(variablesBloc);
    }
    //variablesBloc->afficherTable();    //debug
    return variablesBloc;
} //----- Fin de Méthode

void Bloc::creerAssembleur (TableDesSymboles* variables)
// Algorithme :
//
{
    //initialiser le bp, sp
    //cout<<"creation assembleur bloc debut, number of instuctions: "<<instructions.size()<<endl;   //debug
    bool ret = false;
    for(list<Instruction*>::iterator it=instructions.begin(); it!=instructions.end(); ++it)
    {
        //cout<<"creation assembleur d'une instuction -----------------------------"<<endl;   //debug
        (*it)->creerAssembleur(variablesBloc);
        bool ret = (*it)->isReturn();
        if (ret){
            
            break;
        }
    }
    //cout<<"creation assembleur bloc fin"<<endl;   //debug
    //variablesBloc->afficherTable();    //debug
} //----- Fin de Méthode

void Bloc::ajouterInstruction (Instruction * instruction)
// Algorithme :
//
{
    //ajouter tout ExpressionVar a la TableDesSymboles et ne pas push_back 
    instructions.push_back(instruction);


} //----- Fin de Méthode

//------------------------------------------------- Surcharge d'opérateurs
//Bloc & Bloc::operator = ( const Bloc & unBloc )
// Algorithme :
//
//{
//} //----- Fin de operator =


//-------------------------------------------- Constructeurs - destructeur
//Bloc::Bloc ( const Bloc & unBloc )
// Algorithme :
//
//{
//#ifdef MAP
//    cout << "Appel au constructeur de copie de <InstrRet>" << endl;
//#endif
//} //----- Fin de Bloc (constructeur de copie)


Bloc::Bloc (TableDesSymboles * table)
// Algorithme :
//
{

variablesBloc = table;

#ifdef MAP
    cout << "Appel au constructeur de <Bloc>" << endl;
#endif
} //----- Fin de Bloc


Bloc::~Bloc ( )
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au destructeur de <Bloc>" << endl;
#endif
} //----- Fin de ~Bloc


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées


