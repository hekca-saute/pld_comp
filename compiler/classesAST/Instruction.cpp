/*************************************************************************
                           Instruction  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation de la classe <Instruction> (fichier Instruction.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <iostream>
using namespace std;

//------------------------------------------------------ Include personnel
#include "Variable.h"
#include "TableDesSymboles.h"
#include "Instruction.h"


//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques
// type Instruction::Méthode ( liste des paramètres )
// Algorithme :
//
//{
//} //----- Fin de Méthode

int Instruction::creerEtiquette()
// Algorithme :
//
{
    nombreEtiquettes++;
    return nombreEtiquettes;
} //----- Fin de Méthode



//------------------------------------------------- Surcharge d'opérateurs
//Instruction & Instruction::operator = ( const Instruction & unInstruction )
// Algorithme :
//
//{
//} //----- Fin de operator =


//-------------------------------------------- Constructeurs - destructeur
//Instruction::Instruction ( const Instruction & unInstruction )
// Algorithme :
//
//{
//#ifdef MAP
//    cout << "Appel au constructeur de copie de <Instruction>" << endl;
//#endif
//} //----- Fin de Instruction (constructeur de copie)


Instruction::Instruction ( )
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au constructeur de <Instruction>" << endl;
#endif
} //----- Fin de Instruction


Instruction::~Instruction ( )
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au destructeur de <Instruction>" << endl;
#endif
} //----- Fin de ~Instruction


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées


