/*************************************************************************
                           ExpressionConst  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Interface de la classe <ExpressionConst> (fichier ExpressionConst.h) ----------------
#if ! defined ( EXPRESSIONCONST_H )
#define EXPRESSIONCONST_H

//--------------------------------------------------- Interfaces utilisées

//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <ExpressionConst>
//
//
//------------------------------------------------------------------------

class ExpressionConst : public Expression
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques
    // type Méthode ( liste des paramètres );
    // Mode d'emploi :
    //
    // Contrat :
    //

    string creerAssembleurExpr ( TableDesSymboles * variables );
    // Mode d'emploi :
    //
    // Contrat :
    //
    int isConst ();
    // Mode d'emploi :
    //
    // Contrat :
    //

    TableDesSymboles * parcourAST(TableDesSymboles * tds);
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------- Surcharge d'opérateurs
    ExpressionConst & operator = ( const ExpressionConst & unExpressionConst );
    // Mode d'emploi :
    //
    // Contrat :
    //


//-------------------------------------------- Constructeurs - destructeur

    ExpressionConst (int valeur);
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~ExpressionConst ( );
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE

protected:

//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés
int valeur;
};

//-------------------------------- Autres définitions dépendantes de <ExpressionConst>

#endif // EXPRESSIONCONST_H

