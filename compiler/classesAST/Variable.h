/*************************************************************************
                           Variable  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Interface de la classe <Variable> (fichier Xxx.h) ----------------
#if ! defined ( VARIABLE_H )
#define VARIABLE_H

//--------------------------------------------------- Interfaces utilisées

//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <Variable>
//
//
//------------------------------------------------------------------------

class Variable
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques
    // type Méthode ( liste des paramètres );
    // Mode d'emploi :
    //
    // Contrat :
    //
    void setAdresseMemoire(int offset);
    // Mode d'emploi :
    //
    // Contrat :
    //

    int getOffset ();
    // Mode d'emploi :
    //
    // Contrat :
    //
    std::string getType ();
    // Mode d'emploi :
    //
    // Contrat :
    //

    bool getAEteUtilisee ();
    // Mode d'emploi :
    //
    // Contrat :
    //
    
    void utiliser ();
    // Permet de définir si une vriable a été utilisée ou non. 
    //Utilisée dans la gestion d'erreurs.

    int getLigne();

    int getColonne();

//------------------------------------------------- Surcharge d'opérateurs
    //Xxx & operator = ( const Variable & unVariable );
    // Mode d'emploi :
    //
    // Contrat :
    //


//-------------------------------------------- Constructeurs - destructeur
    //Xxx ( const Variable & unVariable );
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //

    Variable ();
    // Mode d'emploi :
    //
    // Contrat :
    //
    Variable (std::string type, int l, int c);
    // Mode d'emploi :
    //
    // Contrat :
    //

    Variable (std::string type);
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~Variable ( );
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE

protected:
    std::string type;
    int offset;
    bool aEteUtilisee;
    int ligne;
    int colonne;
    
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés

};

//-------------------------------- Autres définitions dépendantes de <Xxx>

#endif // VARIABLE_H

