/*************************************************************************
                           ExpressionConst  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation de la classe <ExpressionConst> (fichier ExpressionConst.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <iostream>
using namespace std;

//------------------------------------------------------ Include personnel
#include "Variable.h"
#include "TableDesSymboles.h"
#include "Instruction.h"
#include "Expression.h"
#include "ExpressionConst.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques

string ExpressionConst::creerAssembleurExpr (TableDesSymboles * variables)
// Algorithme :
//
{
    //cout<<"creation assembleur Const"<<endl;   //debug
    Variable var = Variable("int");
    string varName = to_string(valeur);
    variables->ajouterVariable(varName,var);
    int addresseMemoireCible = variables->getOffset(varName);
    cout<<"movl	$"<<valeur<<", "<<addresseMemoireCible<<"(%rbp)\n";
    return varName;
} //----- Fin de Méthode

int ExpressionConst::isConst ()
{
    return 1;
};

TableDesSymboles * ExpressionConst::parcourAST ( TableDesSymboles * variables )
{
    //cout<<"parcourAST Expr Const"<<endl;    //debut
    variables->incrNombreConstFonction();
    return variables;
}

//------------------------------------------------- Surcharge d'opérateurs
//ExpressionConst & ExpressionConst::operator = ( const ExpressionConst & unExpressionConst )
// Algorithme :
//
//{
//} //----- Fin de operator =


//-------------------------------------------- Constructeurs - destructeur
ExpressionConst::ExpressionConst (int val )
// Algorithme :
//
{
    valeur = val;
#ifdef MAP
    cout << "Appel au constructeur de <ExpressionConst>" << endl;
#endif
} //----- Fin de ExpressionConst



ExpressionConst::~ExpressionConst ( )
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au destructeur de <ExpressionConst>" << endl;
#endif
} //----- Fin de ~ExpressionConst


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

