/*************************************************************************
                             BoucleIf -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Interface de la classe <BoucleIf> (fichier BoucleIf.h) ----------------
#if ! defined ( BOUCLEIF_H )
#define BOUCLEIF_H


//--------------------------------------------------- Interfaces utilisées
#include <utility> 
#include <list>
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <BoucleIf>
//
//
//------------------------------------------------------------------------

class BoucleIf : public Instruction 
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques
    // type Méthode ( liste des paramètres );
    // Mode d'emploi :
    //
    // Contrat :
    //
    void creerAssembleur (TableDesSymboles * variables);
    // Mode d'emploi :
    //
    // Contrat :
    //
    
     TableDesSymboles * parcourAST(TableDesSymboles * tds);
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------- Surcharge d'opérateurs
    //BoucleIf & operator = ( const BoucleIf & unBoucleIf );
    // Mode d'emploi :
    //
    // Contrat :
    //


//-------------------------------------------- Constructeurs - destructeur
    //BoucleIf ( const BoucleIf & unBoucleIf );
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //
    
    BoucleIf (list<pair<Expression*,Bloc*>> blocs);
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~BoucleIf ( );
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE

protected:
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés
list<pair<Expression*,Bloc*>> blocs;
string etiquetteFin;
};

//-------------------------------- Autres définitions dépendantes de <BoucleIf>

#endif // BOUCLEIF_H


