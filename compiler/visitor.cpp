
// Generated from ifcc.g4 by ANTLR 4.7.2


#include "visitor.h"

antlrcpp::Any Visitor::visitAxiom(ifccParser::AxiomContext *ctx) {
    Prog * programme = visit(ctx->prog()).as<Prog*>();
    gestionnaire.afficherErreurs();
    if(gestionnaire.contientErreurFatale()){
      exit(-1);
    }
    return programme;
}

antlrcpp::Any Visitor::visitProg(ifccParser::ProgContext *ctx) {
    Prog * programme = new Prog();
    std::vector<ifccParser::DefFonctionContext *> fonctions= ctx->defFonction();
    for(std::vector<ifccParser::DefFonctionContext *>::iterator it=fonctions.begin(); it!=fonctions.end(); ++it)
    {
        programme->ajouterFonction(visit(*it).as<DefFonction*>());
    }
    return programme;
}

antlrcpp::Any Visitor::visitDefFonction(ifccParser::DefFonctionContext *ctx) {
    variablesFonctionCourante = nullptr;
    string funcName = ctx->ID()->getText();
    string funcType = ctx->TYPE()->getText();
    std::vector<pair<std::string, std::string>> paramFonction = visit(ctx->paramfonction()).as<std::vector<pair<std::string, std::string>>>();
    Bloc * bloc = visit(ctx->block()).as<Bloc*>();
    DefFonction * fonction = new DefFonction(funcName, funcType, bloc);
    fonction->setParamFonction(paramFonction);
    return fonction;
}

antlrcpp::Any Visitor::visitBlock(ifccParser::BlockContext *ctx) {
    TableDesSymboles * tmp = variablesFonctionCourante;
    variablesFonctionCourante = new TableDesSymboles(tmp);
    Bloc * bloc = new Bloc(variablesFonctionCourante);

    if (tmp==nullptr)
    {
      for(std::vector<std::pair<string, string>>::iterator it=parametresFonctionCourante.begin(); it!=parametresFonctionCourante.end(); ++it)
      {
        string nomVar = (*it).first;
        string type = (*it).second;
        Variable var = Variable(type);
        bool retour = variablesFonctionCourante->ajouterVariable(nomVar, var);
        variablesFonctionCourante->utiliserVariable(nomVar);
      }
    }

    std::vector<ifccParser::DeclarationContext *> declarations= ctx->declaration();
    for(std::vector<ifccParser::DeclarationContext *>::iterator it=declarations.begin(); it!=declarations.end(); ++it)
    {
      visit(*it);
    }

    std::vector<ifccParser::InstructionContext *> instructions= ctx->instruction();
    for(std::vector<ifccParser::InstructionContext *>::iterator it=instructions.begin(); it!=instructions.end(); ++it)
    {
      Instruction * instruction = (Instruction*)visit(*it);
      bloc->ajouterInstruction(instruction);
    }

    unordered_map<string,Variable> verificationUtilisation = variablesFonctionCourante->verifierUtilisation();
        if(!verificationUtilisation.empty()){
          for(auto it:verificationUtilisation){
            Erreur * e = new ErreurVariableNonUtilisee(it.second.getLigne(), it.second.getColonne(), it.first);
            gestionnaire.ajouterErreur(e);
          }
        }
    variablesFonctionCourante = tmp;
    return bloc;
}

antlrcpp::Any Visitor::visitParamFonction(ifccParser::ParamFonctionContext *ctx)  {

    std::vector<antlr4::tree::TerminalNode *> types= ctx->TYPE();
    std::vector<antlr4::tree::TerminalNode *> nomVars = ctx->ID();
    std::vector<std::pair<string, string>> params;
    for(int i = 0; i<types.size();i++)
    {
      std::pair<string, string> param (nomVars[i]->getText(), types[i]->getText());
      params.push_back(param);
    }
    parametresFonctionCourante = params;

    return params;
}

antlrcpp::Any Visitor::visitInstruDecAff(ifccParser::InstruDecAffContext *ctx) {
    string type = ctx->TYPE()->getText();
    string nomVar = ctx->ID()->getText();
    Variable var = Variable(type);

    bool retour = variablesFonctionCourante->ajouterVariable(nomVar, var);
    if (!retour){
        int l = ctx->ID()->getSymbol()->getLine();
        int c = ctx->ID()->getSymbol()->getCharPositionInLine() + 1;
        Erreur * e = new ErreurDoubleDeclaration(l, c, nomVar);
        gestionnaire.ajouterErreur(e);
    }
    variablesFonctionCourante->utiliserVariable(nomVar);

    Expression * expr = visit(ctx->expr()).as<Expression*>();
    Expression * exprVar = new ExpressionVar(nomVar);
    Instruction * exprAff = new ExpressionOperation("=",exprVar,expr);
    return exprAff;
}

antlrcpp::Any Visitor::visitInstruExpr(ifccParser::InstruExprContext *ctx) {
    Instruction * instru = visit(ctx->expr()).as<Expression*>();
    return instru;
}

antlrcpp::Any Visitor::visitInstruBlock(ifccParser::InstruBlockContext *ctx) {
    Instruction * bloc = visit(ctx->block()).as<Bloc*>();
    return bloc;
}

antlrcpp::Any Visitor::visitInstrRet(ifccParser::InstrRetContext *ctx) {
    Instruction * instr = visit(ctx->ret()).as<Instruction*>();
    return instr;
}

antlrcpp::Any Visitor::visitInstrIf(ifccParser::InstrIfContext *ctx) {
      list<pair<Expression*,Bloc*>> blocs = visit(ctx->instruIf()).as<list<pair<Expression*,Bloc*>>>();
      Instruction * instruIf = new BoucleIf(blocs);
      return instruIf;
}

antlrcpp::Any Visitor::visitInstrFor(ifccParser::InstrForContext *ctx) {
    Instruction * instruFor = visit(ctx->instruFor()).as<Instruction*>();
    return instruFor;
}

antlrcpp::Any Visitor::visitInstrWhile(ifccParser::InstrWhileContext *ctx) {
    Instruction * instruWhile = visit(ctx->instruWhile()).as<Instruction*>();
    return instruWhile;
}

antlrcpp::Any Visitor::visitDefIf(ifccParser::DefIfContext *ctx) {
    Expression * condition = visit(ctx->expr()).as<Expression*>();
    Bloc * bloc1 = visit(ctx->block()).as<Bloc*>();
    pair<Expression*, Bloc*> blocIf(condition, bloc1);
    list<pair<Expression*,Bloc*>> blocs = visit(ctx->instruElse()).as<list<pair<Expression*,Bloc*>>>();
    blocs.push_front(blocIf);
    return blocs;
}

antlrcpp::Any Visitor::visitDefElse(ifccParser::DefElseContext *ctx) {
    list<pair<Expression*,Bloc*>> param;
    Bloc * bloc = visit(ctx->block()).as<Bloc*>();
    Expression * exprVrai = new ExpressionConst(1);
    pair<Expression*, Bloc*> blocElse(exprVrai, bloc);
    param.push_back(blocElse);
    return param;
}

antlrcpp::Any Visitor::visitDefElseIf(ifccParser::DefElseIfContext *ctx) {
    list<pair<Expression*,Bloc*>> blocs = visit(ctx->instruIf()).as<list<pair<Expression*,Bloc*>>>();
    return blocs;
}

antlrcpp::Any Visitor::visitNoElse(ifccParser::NoElseContext *ctx) {
    list<pair<Expression*,Bloc*>> blocs;
    return blocs;
}

antlrcpp::Any Visitor::visitDefFor(ifccParser::DefForContext *ctx) {
    Bloc * bloc = visit(ctx->block()).as<Bloc*>();
    Expression * expr1 = visit(ctx->expr(0)).as<Expression*>();
    Expression * expr2 = visit(ctx->expr(1)).as<Expression*>();
    Expression * expr3 = visit(ctx->expr(2)).as<Expression*>();
    Instruction * instruFor = new BoucleFor(expr1,expr2,expr3,bloc);
    return instruFor;
}

antlrcpp::Any Visitor::visitDefWhile(ifccParser::DefWhileContext *ctx) {
    Bloc * bloc = visit(ctx->block()).as<Bloc*>();
    Expression * expr = visit(ctx->expr()).as<Expression*>();
    Instruction * instruWhile = new BoucleWhile(expr,bloc);
    return instruWhile;
}

antlrcpp::Any Visitor::visitRet(ifccParser::RetContext *ctx) {
    Expression * expr = (Expression*)visit(ctx->expr());
    Instruction * instruRet = new InstruRet(expr);
    return instruRet;
}

antlrcpp::Any Visitor::visitDecVar(ifccParser::DecVarContext *ctx) {
    string type = ctx->TYPE()->getText();
    std::vector<antlr4::tree::TerminalNode*> variables= ctx->ID();
    for(std::vector<antlr4::tree::TerminalNode*>::iterator it=variables.begin(); it!=variables.end(); ++it)
    {
       string nomVar = (*it)->getText();
       int l = (*it)->getSymbol()->getLine();
       int c = (*it)->getSymbol()->getCharPositionInLine() + 1;
       Variable var = Variable(type, l, c);
       bool retour = variablesFonctionCourante->ajouterVariable(nomVar, var);
       if (!retour){
        Erreur * e = new ErreurDoubleDeclaration(l, c, nomVar);
        gestionnaire.ajouterErreur(e);
       }
    }
    return 0;
}

antlrcpp::Any Visitor::visitDecFonction(ifccParser::DecFonctionContext *ctx) {
    return visitChildren(ctx);
}

antlrcpp::Any Visitor::visitParamAppelFonction(ifccParser::ParamAppelFonctionContext *ctx) {
    std::vector<ifccParser::ExprContext *> parametres= ctx->expr();
    std::vector<Expression*> param;
    if(parametres.size()!=0){
      int i = 0;
      for(std::vector<ifccParser::ExprContext *>::iterator it=parametres.begin(); it!=parametres.end(); ++it)
      {
        param.push_back(visit(*it).as<Expression*>());
        i++;
      }
    }
    return param;
}


antlrcpp::Any Visitor::visitLeftValue(ifccParser::LeftValueContext *ctx) {

    string nomVar = ctx->ID()->getSymbol()->getText();
    if(!variablesFonctionCourante -> utiliserVariable(nomVar)){
      int l = ctx->ID()->getSymbol()->getLine();
      int c = ctx->ID()->getSymbol()->getCharPositionInLine() + 1;
      Erreur * e = new ErreurVariableNonDeclaree(l, c, nomVar);
      gestionnaire.ajouterErreur(e);
    }
    Expression * exprVar = new ExpressionVar(nomVar);
    return exprVar;
}


antlrcpp::Any Visitor::visitAppelFonction(ifccParser::AppelFonctionContext *ctx) {
    string nomFonction = ctx->ID()->getText()+ "@PLT";
    std::vector<Expression*> parametres = visit(ctx->paramappelfonction()).as<std::vector<Expression*>>();
    Variable varData = Variable("int");
    variablesFonctionCourante->ajouterVariable("_ret", varData);
    variablesFonctionCourante->utiliserVariable("_ret");
    Expression * fonction = new AppelFonction(nomFonction, parametres);
    return fonction;
}

antlrcpp::Any Visitor::visitExprAppelFonction(ifccParser::ExprAppelFonctionContext *ctx) {
    //std::cout<<"we are visiting Expr Appel Fonction"<<std::endl;  //debug
    //AppelFonctionContext *appelFonction();
    Expression * fonction = visit(ctx->appelFonction()).as<Expression*>();
    return fonction;
}

antlrcpp::Any Visitor::visitPar(ifccParser::ParContext *ctx) {
    Expression * expr = visit(ctx->expr()).as<Expression*>();
    return expr;
}

antlrcpp::Any Visitor::visitMultdiv(ifccParser::MultdivContext *ctx) {
    string op = ctx->OPMULTDIV()->getText();
    Expression * expr1= visit(ctx->expr(0)).as<Expression*>();
    Expression * expr2= visit(ctx->expr(1)).as<Expression*>();
    Expression * expr = new ExpressionOperation(op,expr1,expr2);
    return expr;
}

antlrcpp::Any Visitor::visitComparaison(ifccParser::ComparaisonContext *ctx) {
    string op = ctx->OPCOMPARAISON()->getText();
    Expression * expr1= visit(ctx->expr(0)).as<Expression*>();
    Expression * expr2= visit(ctx->expr(1)).as<Expression*>();
    Expression * expr = new ExpressionOperation(op,expr1,expr2);
    return expr;
}

antlrcpp::Any Visitor::visitConst(ifccParser::ConstContext *ctx) {
    int valeur = stoi(ctx->INT()->getText());
    Expression * expr = new ExpressionConst(valeur);
    return expr;
}

antlrcpp::Any Visitor::visitVar(ifccParser::VarContext *ctx) {
    string nomVar = ctx->ID()->getText();
    Expression * expr = new ExpressionVar(nomVar);
    if(!variablesFonctionCourante -> utiliserVariable(nomVar)){
      int l = ctx->ID()->getSymbol()->getLine();
      int c = ctx->ID()->getSymbol()->getCharPositionInLine() + 1;
      Erreur * e = new ErreurVariableNonDeclaree(l, c, nomVar);
      gestionnaire.ajouterErreur(e);
    }
    return expr;
}

antlrcpp::Any Visitor::visitPlusmoins(ifccParser::PlusmoinsContext *ctx) {
    string op = ctx->OPPLUSMOINS()->getText();
    Expression * expr1= visit(ctx->expr(0)).as<Expression*>();
    Expression * expr2= visit(ctx->expr(1)).as<Expression*>();
    Expression * expr = new ExpressionOperation(op,expr1,expr2);
    return expr;
}

antlrcpp::Any Visitor::visitAff(ifccParser::AffContext *ctx) {
    Expression * exprVar = visit(ctx->lvalue()).as<Expression*>();
    Expression * expr = visit(ctx->expr()).as<Expression*>();
    Expression * exprAff = new ExpressionOperation("=",exprVar,expr);
    return exprAff;
}



