
// Generated from ifcc.g4 by ANTLR 4.7.2

#pragma once


#include "antlr4-runtime.h"
#include "antlr4-generated/ifccBaseVisitor.h"
#include "classesAST/Variable.h"
#include "classesAST/TableDesSymboles.h"
#include "classesAST/Instruction.h"
#include "classesAST/Expression.h"
#include "classesAST/ExpressionConst.h"
#include "classesAST/ExpressionOperation.h"
#include "classesAST/ExpressionVar.h"
#include "classesAST/Instruction.h"
#include "classesAST/Bloc.h"
#include "classesAST/InstruRet.h"
#include "classesAST/BoucleIf.h"
#include "classesAST/BoucleFor.h"
#include "classesAST/BoucleWhile.h"
#include "classesAST/AppelFonction.h"
#include "classesAST/DefFonction.h"
#include "classesAST/Prog.h"

#include "Erreur/Erreur.h"
#include "Erreur/ErreurDoubleDeclaration.h"
#include "Erreur/ErreurVariableNonUtilisee.h"
#include "Erreur/ErreurVariableNonDeclaree.h"
#include "Erreur/GestionnaireErreurs.h"
#include <utility> 
#include <list>

/**
 * This class provides an empty implementation of ifccVisitor, which can be
 * extended to create a visitor which only needs to handle a subset of the available methods.
 */
class  Visitor : public ifccVisitor {

protected:
TableDesSymboles * variablesFonctionCourante;
GestionnaireErreurs gestionnaire;
std::vector<std::pair<string, string>> parametresFonctionCourante; //nom //type

public:


  virtual antlrcpp::Any visitAxiom(ifccParser::AxiomContext *ctx) override;

  virtual antlrcpp::Any visitProg(ifccParser::ProgContext *ctx) override;

  virtual antlrcpp::Any visitDefFonction(ifccParser::DefFonctionContext *ctx) override;

  virtual antlrcpp::Any visitBlock(ifccParser::BlockContext *ctx) override;

  virtual antlrcpp::Any visitParamFonction(ifccParser::ParamFonctionContext *ctx) override;

   virtual antlrcpp::Any visitInstruDecAff(ifccParser::InstruDecAffContext *ctx) override;

  virtual antlrcpp::Any visitInstruExpr(ifccParser::InstruExprContext *ctx) override;

  virtual antlrcpp::Any visitInstruBlock(ifccParser::InstruBlockContext *ctx) override;

  virtual antlrcpp::Any visitInstrRet(ifccParser::InstrRetContext *ctx) override;

  virtual antlrcpp::Any visitInstrIf(ifccParser::InstrIfContext *ctx) override;

  virtual antlrcpp::Any visitInstrFor(ifccParser::InstrForContext *ctx) override;

  virtual antlrcpp::Any visitInstrWhile(ifccParser::InstrWhileContext *ctx) override;

  virtual antlrcpp::Any visitDefIf(ifccParser::DefIfContext *ctx) override;

  virtual antlrcpp::Any visitDefElse(ifccParser::DefElseContext *ctx) override;

  virtual antlrcpp::Any visitDefElseIf(ifccParser::DefElseIfContext *ctx) override;

  virtual antlrcpp::Any visitNoElse(ifccParser::NoElseContext *ctx) override;

  virtual antlrcpp::Any visitDefFor(ifccParser::DefForContext *ctx) override;

  virtual antlrcpp::Any visitDefWhile(ifccParser::DefWhileContext *ctx) override;

  virtual antlrcpp::Any visitRet(ifccParser::RetContext *ctx) override;

  virtual antlrcpp::Any visitDecVar(ifccParser::DecVarContext *ctx) override;

  virtual antlrcpp::Any visitDecFonction(ifccParser::DecFonctionContext *ctx) override;

  virtual antlrcpp::Any visitParamAppelFonction(ifccParser::ParamAppelFonctionContext *ctx) override;

  virtual antlrcpp::Any visitLeftValue(ifccParser::LeftValueContext *ctx) override;

  virtual antlrcpp::Any visitAppelFonction(ifccParser::AppelFonctionContext *ctx) override;

  virtual antlrcpp::Any visitExprAppelFonction(ifccParser::ExprAppelFonctionContext *ctx) override;

  virtual antlrcpp::Any visitPar(ifccParser::ParContext *ctx) override;

  virtual antlrcpp::Any visitMultdiv(ifccParser::MultdivContext *ctx) override;

  virtual antlrcpp::Any visitComparaison(ifccParser::ComparaisonContext *ctx) override;

  virtual antlrcpp::Any visitConst(ifccParser::ConstContext *ctx) override;

  virtual antlrcpp::Any visitVar(ifccParser::VarContext *ctx) override;

  virtual antlrcpp::Any visitPlusmoins(ifccParser::PlusmoinsContext *ctx) override;

  virtual antlrcpp::Any visitAff(ifccParser::AffContext *ctx) override;

};

