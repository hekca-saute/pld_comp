grammar ifcc; 

axiom : prog       
    ;

prog : defFonction+
    ;

defFonction : TYPE ID '('paramfonction')' block 
        ;
paramfonction: (TYPE ID (',' TYPE ID)*)?    #ParamFonction
        ;

TYPE: 'int'         
    | 'char'
    | 'void'        
    ;
block: '{' declaration* instruction* '}'
    ;
instruction: TYPE ID '=' expr ';'   # InstruDecAff
            |expr ';'               # InstruExpr
            |block                  # InstruBlock
            |ret                    # InstrRet
            |instruIf               # InstrIf
            |instruFor              # InstrFor
            |instruWhile            # InstrWhile
    ;

instruIf: 'if' '(' expr ')' block instruElse #DefIf
    ;

instruElse: 'else' block #DefElse
    | 'else' instruIf #DefElseIf
    | #NoElse
    ;

instruFor: 'for' '(' expr ';' expr ';' expr ')' block #DefFor
    ;

instruWhile: 'while' '(' expr ')' block #DefWhile
    ;

ret : 'return' expr ';'
    ;
declaration : TYPE ID (','ID)* ';'                # decVar
            | TYPE ID '(' paramfonction ')' ';'   # decFonction
            ;
paramappelfonction:   (expr (',' expr)*)?   # ParamAppelFonction
        ;
INT : '-'? [0-9]+
    ;
ID : [a-zA-Z0-9]+
    ;

expr: expr OPMULTDIV expr     # multdiv
    | expr OPPLUSMOINS expr   # plusmoins
    | expr OPCOMPARAISON expr #comparaison
    | appelFonction           #exprAppelFonction
    | ID                      # var
    | INT                     # const
    | '(' expr ')'            # par
    | lvalue '=' expr         # aff
    ;

appelFonction: ID '(' paramappelfonction ')'
    ;

lvalue: ID      #leftValue
    ;

    
OPMULTDIV : '*' | '/'
    ;
OPPLUSMOINS : '+' | '-'
    ;
OPCOMPARAISON : '<' | '<=' | '>' | '>=' | '==' | '!='
    ;
    
COMMENT : '/*' .*? '*/' -> skip
    ;
LIGNECOM : '//' .*? '\n' -> skip
    ;
DIRECTIVE : '#' .*? '\n' -> skip
    ;
WS  : [ \t\r\n] -> channel(HIDDEN)
    ;
