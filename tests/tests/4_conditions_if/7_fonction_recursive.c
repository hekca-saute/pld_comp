int main () {
    int n = 5;
    int res = factorielle (n);
    return res;
}

int factorielle (int n) {
    if (n >= 1) {
        return n*factorielle (n - 1);
    } else {
        return 1;
    }
}