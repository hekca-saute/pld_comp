int factorielle (int n) {
    if (n >= 1) {
        return n * factorielle( n - 1 );
    } else {
        return 1;
    }
}

int main () {
    int n = 5;
    int a = factorielle(n);
    return a;
}