int main()
{
  int suivant, i;
  int nbr1 = 0;
  int nbr2 = 1;
  int n = 6;
 
  for (i = 0; i < n; i=i+1)
  {
      if (i <= 1)
      {
          suivant = i;
      }
      else
      {
          suivant = nbr1 + nbr2;
          nbr1 = nbr2;
          nbr2 = suivant;
      }
  }
 
  return suivant;
}