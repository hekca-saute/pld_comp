# C Compiler test framework

This directory contains scripts to test your compiler along with a collection of C language source codes that can be tested.

## Prerequisites

Docker must be installed in order to execute these scripts. The compiler has to be present in the directory `../compiler` and you need to have the executable `ifcc` in this folder. Compiler options and location can be customized in the `pld-wrapper.sh` script.

## Files

Tests files are in the `/tests/` folder, and are separated in different types of tests (programs with loops, programs with functions...).

`test.sh` is the base file to launch. It will call the appropriate Docker image and execute the `pld-test.py` script. Results are put in a new directory `pld-test-outputs`. The python script will go through each folder in the `tests` folder and test each program in those folder.

If you want to run a test on a specific folder or file you need to use the command (don't forget to change folder and file names):

* testing a single file : `docker run --rm -v $(pwd):/work eguerin/antlr4cpp bash -c "cd /work/tests ; chmod 755 pld-wrapper.sh; python3 pld-test.py -v -v tests/folder_name/file_name"`
* testing all files in a folder : `docker run --rm -v $(pwd):/work eguerin/antlr4cpp bash -c "cd /work/tests ; chmod 755 pld-wrapper.sh; python3 pld-test.py -v -v tests/folder_name"`

If you want to test a single file by yourself you can compile it with `gcc` and `ifcc` (don't forget to use docker if you compiled `ifcc` with it), then run the 2 executable files you obtained and compare the returned values by doing `echo $?` after running a program.

There is a CI/CD in this repo on the branch `master`, if the `build` stage is a success, it will run in parallel a test job for each folder in the `/tests/` folder.

> Some tests files might look dumb or not useful, but the goal is to test a maximum of C functionalities so some test programs are really simple and sometimes look stupid.

## Tests list

Here is a little recap of the tests files (to read easily, underscores and ".c" in file names have been removed):

| Folder                  |               File name                |                   Tested feature                    |                   Expected result |
| :---------------------- | :------------------------------------: | :-------------------------------------------------: | --------------------------------: |
| **1_static**            |                1 ret42                 |                   a simple return                   |                         return 42 |
|                         |    2 declaration definition simple     |          declaring and defining a variable          |                         return 42 |
|                         |           3 return variable            |                                                     |                         return 42 |
|                         |         4 variable to variable         |            putting a variable in another            |                         return 12 |
|                         |          5 multi_declaration           |       declaring several variables in one line       |                         return 12 |
|                         |           6 return_negative            |                                                     |                        return -42 |
|                         |     7 fail missing curly brackets      |                                                     |             an error at compiling |
|                         |        8 fail missing semicolon        |                                                     |             an error at compiling |
|                         |  9 fail return non declared variable   |                                                     |             an error at compiling |
|                         |       10 fail double declaration       |                                                     |             an error at compiling |
|                         |          11 fail return void           | an error returning a void instead of an int in main |             an error at compiling |
|                         |      12 fail missing parentheses       |                                                     |             an error at compiling |
|                         |           13 unused variable           |                                                     | a warn at compiling and return 42 |
|                         |        14 commentaire une ligne        |                                                     |                         return 42 |
|                         |  15 commentaire sur plusieurs lignes   |                                                     |                         return 42 |
|                         |       16 fail double definition        |                                                     |             an error at compiling |
| **2_expr_arithmetique** |           1 simple addition            |                                                     |                          return 5 |
|                         |         2 simple soustraction          |                                                     |                         return 42 |
|                         |       3 addition et soustraction       |                                                     |                          return 4 |
|                         |        4 multiplication simple         |                                                     |                          return 6 |
|                         |               5 priorite               |         check math priorities are respected         |                         return 14 |
|                         |         6 priorite parentheses         |           idem as before with parentheses           |                         return 20 |
|                         |            7 calcul general            |           a general arithmetic expression           |                         return 10 |
| **3_fonctions**         |     1 declaration fonction basique     |              just declaring a function              |                         return 42 |
|                         |        2 appel fonction simple         |               calling a sum function                |                          return 3 |
|                         |    3 appel fonction avec variables     |        put variables in function parameters         |                         return 15 |
|                    |    4 fail fonction sans identifiant    |                                                     |             an error at compiling |
|                    |          5 fail oubli param          |                                                     |             an error at compiling |
|                         |              6 procedure              |                   a void function                   |                         return 42 |
|                    |     7 fail fonction sans accolade     |                                                     |                error at compiling |
| | 8 putchar | print a char | print '<' and return 0 |
| **4_conditions_if**     |              1 simple if               |                  an always true if                  |                         return 42 |
|                         |          2 if avec condition           |             if with a simple comparison             |                          return 5 |
|                         |               3 if else                |                                                     |                         return 12 |
|                         |         4 if else avec return          |              returns inside if...else               |                         return 20 |
|                         |                 5 elif                 |                                                     |                         return 17 |
|                         |             6 if imbrique              |                  if inside another                  |                         return 25 |
|  |          7 fonction recursive          |      a function calling itself thanks to an if      |                        return 120 |
|                         |        8 fail if sans accolade        |                                                     |                error at compiling |
|                         |          9 fail else sans if          |                                                     |                error at compiling |
|                         |           10 fail elif seul           |                                                     |                error at compiling |
|                         |       11 fail if sans condition       |                                                     |                error at compiling |
|                         |      12 fail if sans parentheses      |                                                     |                error at compiling |
| **5_boucles**           |             1 while basique             |                                                     |                         return 12 |
|                         |       2 fail while sans condition       |                                                     |                error at compiling |
|                    |       3 fail while oubli accolade       |                                                     |                error at compiling |
|                         |               4 for simple               |                                                     |                         return 87 |
|                         |         5 fail for avec virgules         |                                                     |                error at compiling |
|                         |         6 fail for i non declare         |                                                     |                error at compiling |
|                         |       7 fail for sans parentheses       |                                                     |                error at compiling |
|                         |       8 fail while sans parenthese       |                                                     |                error at compiling |
| **6_e2e**              |           1 factorielle iterative           |             compute 5! with a for loop              |                        return 120 |
|                         |        2 factorielle recursive         |        compute 5! with a recursive function         |                        return 120 |
| |              3 fibonacci               | return 6th number of fibonacci suite | return 5 |
| | 4 affiche alphabet | print the alphabet | print 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' and return 0 |
