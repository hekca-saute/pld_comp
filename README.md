# PLD COMP

* [How to compile](#How-to-compile)
* [How to use this git repo](#How-to-use-this-git-repo)
* [How to run tests](#How-to-run-tests)

This a school project at INSA Lyon, the goal is to create a C compiler usins antlr.

## How to compile

If you have the right tools installed on your computer (antlr...) you can use directly the makefile with the command ```make```
But if you don't have the required tools installed you can use docker to compile. For this you need to have Docker installed on your machine and run the script with ```./compile_docker.sh```, the script will use a Docker image created by our teacher and thn use the makefile to compile.
Warning : it might be possible that the binary file created by the compile_docker.sh is only runnable in the docker environment, so you would need to launch the container to execute the file in it.

## How to use this git repo

To avoid executing pipelines at each push on gitlab, we will mainly word on the branch ```develop``` and when tere is a need to execute the pipelines there is just a ```git merge``` on branch master to do.
But before merging on the branch master you need to check that your version og the project is compiling, to avoid running pipelines for nothing.

> If you have doubt about the use of git and branches you go on this site : [learn git branches](https://learngitbranching.js.org/), you won't learn how to use git command line with this site but you will understant better how certain commands works and how git works.

## How to run tests

If you want to see if the compiler works, you can put examples of files in the ```tests/tests/``` folder. Then there is 2 scripts, one if you compiled directly on your machine without docker : ```./test_if.sh```. And one to run tests with docker if you compiled on docker : ```test.sh```.
Those scripts will do almost the same thing, using the script ```pld-test.py``` it will compile all of the files to test with gcc and with our compiler, it will then compare the return codes of compiling,linking and executing of both compiler to check if our compiler works fine or not.

## How to run test individually

Go just under the root of the PLD_COMP and execute this command.
A test.c must be added to the compiler directory.

```docker run --rm -v $(pwd):/work eguerin/antlr4cpp bash -c "cd /work/compiler; ./ifcc test.c"```
